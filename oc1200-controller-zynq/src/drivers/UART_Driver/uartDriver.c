/******************************************************************************
*
* Copyright (C) 2010 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/
/****************************************************************************/
/**
*
* @file		xuartps_intr_example.c
*
* This file contains a design example using the XUartPs driver in interrupt
* mode. It sends data and expects to receive the same data through the device
* using the local loopback mode.
*
*
* @note
* The example contains an infinite loop such that if interrupts are not
* working it may hang.
*
* MODIFICATION HISTORY:
* <pre>
* Ver   Who    Date     Changes
* ----- ------ -------- ----------------------------------------------
* 1.00a  drg/jz 01/13/10 First Release
* 1.00a  sdm    05/25/11 Modified the example for supporting Peripheral tests
*		        in SDK
* 1.03a  sg     07/16/12 Updated the example for CR 666306. Modified
*			the device ID to use the first Device Id
*			and increased the receive timeout to 8
*			Removed the printf at the start of the main
*			Put the device normal mode at the end of the example
* 3.1	kvn		04/10/15 Added code to support Zynq Ultrascale+ MP.
* 3.1   mus     01/14/16 Added support for intc interrupt controller
*
* </pre>
****************************************************************************/

/***************************** Include Files *******************************/

#include "xparameters.h"
#include "xplatform_info.h"
#include "xuartps.h"
#include "xil_exception.h"
#include "xil_printf.h"
#include "stdbool.h"
#include "Xil_io.h"

//#include "MdioMstr.h"
#include "platform.h"
#include "errors.h"
#include "FreeRTOS.h"
#include "task.h"
#ifdef XPAR_INTC_0_DEVICE_ID
#include "xintc.h"
#else
#include "xscugic.h"
#endif
#include "uartDriver.h"
#include "../TLP_Driver/tlpDriver.h"


/************************** Constant Definitions **************************/


/*
 * The following constant controls the length of the buffers to be sent
 * and received with the UART,
 */
#define TEST_BUFFER_SIZE	50


/**************************** Type Definitions ******************************/




/************************** Variable Definitions ***************************/
static void prvRxTask( void *pvParameters );
static TaskHandle_t xRxTask;
XUartPs UartPs	;		/* Instance of the UART Device */
INTC InterruptController;	/* Instance of the Interrupt Controller */


/*
 * The following buffers are used in this example to send and receive data
 * with the UART.
 */
static u8 SendBuffer[TEST_BUFFER_SIZE];	/* Buffer for Transmitting Data */
static u8 RecvBuffer[TEST_BUFFER_SIZE];	/* Buffer for Receiving Data */
uint8_t AscCnv(uint8_t conv);
void CalcCheck(uint8_t * Buff, uint16_t len);
void Conv16bToAsc(uint16_t val,uint8_t * sdbuf);
bool RxChecksum(uint8_t * rxb, uint16_t len);
/*
 * The following counters are used to determine when the entire buffer has
 * been sent and received.
 */
volatile int TotalReceivedCount;
volatile int TotalSentCount;
int TotalErrorCount;
volatile bool UartPrcess = false;

errors_t InitUart( void )
{
	bool UartInitOk = true;
    portBASE_TYPE status;


	/* Start MZ controls task */
    status = xTaskCreate( prvRxTask, (const char *)"RX CTRL ",
	    		configMINIMAL_STACK_SIZE, NULL, UARTRX_PRIORITY, NULL );

	    /* Return status */
     return ( (status == pdPASS) && UartInitOk ) ? ERR_NONE : ERR_INITIALISE_FAILED;



}
static void prvRxTask( void *pvParameters )
{

	 UartInit(&InterruptController, &UartPs,
	    				UART_DEVICE_ID, UART_INT_IRQ_ID);


	while (1) {

	UartTask(&UartPs);
	}
}

void ToggleLed(uint16_t led)
{
	static uint16_t ledstate=0;
	ledstate=~ledstate&(1<<led);
	/* leds are at bits 0 to 3 */

	Xil_Out32(STD_SCRATCH_ADDR0, (uint32_t)ledstate);
}

#define INDIRECT_MODE_WRITE_REG_ADDR		(0x31)
#define INDIRECT_MODE_WRITE_REG_DATA		(0x32)
#define INDIRECT_MODE_READ_REG_ADDR			(0x33)
#define INDIRECT_MODE_READ_POLL_RESULT		(0x34)

void ProcessUart ( XUartPs *UartInstPtr )
{
	u32 bytcount=0;
	//MDIOPKTR mdiRd;
	//MDIOWRPKT mdiWr;
	//mdiRd.mdioPrt = MDIOMST1;
	//mdiWr.mdioport = MDIOMST1;
	int32_t local_16ut;
	//uint16_t Wr_Data[MAX_WR_SIZE];
	uint16_t cnt = 0;
	uint8_t flt = 0;
	uint32_t data = 0x00;
	bool waitForReply = false;
	uint32_t addr = 0x00;

	if(RecvBuffer[0x00] == '<')
	{
		switch(RecvBuffer[0x01])
		{
		case INDIRECT_MODE_WRITE_REG_ADDR:	// set the address for the register to be written - 1/2 part operation
			break;
		case INDIRECT_MODE_WRITE_REG_DATA:	// set the data for the register to be written - 2/2 part operation
			break;
		case INDIRECT_MODE_READ_REG_ADDR:	// set the address of the register from which data is to be read
											// instigates read operation of hardware
											// updates shadow ram and clears bit[16?]
			break;
		case INDIRECT_MODE_READ_POLL_RESULT:	// should never get here - handled by the packet processor and shadow ram
			break;
		default:
			break;
		}	// end of switch(RecvBuffer[0x01])
	}	// end of if(RecvBuffer[0x00] == '<')

#if 0

	if(RecvBuffer[0x00] == '<')
	{
		if((RecvBuffer[0x01] == 0x31) || (RecvBuffer[0x01] == 0x32))
		{
			uint32_t dataLength = (RecvBuffer[0x02] * 256) + RecvBuffer[0x03];
			if(RecvBuffer[0x04 + dataLength])
			{	// TLP message received - place it in TLP block RAM for use by TLP Driver
				uint8_t j = 0x00;
				for(uint16_t i = 0x04; i < dataLength + 4;)
				{
					data = (RecvBuffer[i++] << 24);
					data = data + (RecvBuffer[i++] << 16);
					data = data + (RecvBuffer[i++] << 8);
					data = data + RecvBuffer[i++];

					Xil_Out32(XPAR_BRAM_0_BASEADDR+j, data);

					j=j+4;

					// TODO mark data in shadow ram to indicate read is not complete 0x00010000
					// TLP Register BRAM block 0x4000 0000 - 0x4000 1FFF
					// Shadow BRAM block 0x4200 0000 - 0x4200 1FFF
					// Mapping
					// 0x10FE0 - 0x0FE0 = NTWK_LN0_ACO_R_ADD
					// 0x10FE4 - 0x0FE4 = NTWK_LN0_ACO_R_DATA
					// 0x10FE8 - 0x0FE8 = NTWK_LN0_ACO_W_ADD
					// 0x10FEC - 0x0FEC = NTWK_LN0_ACO_W_DATA
					// 0x10FF0 - 0x0FF0 = NTWK_LN1_ACO_R_ADD
					// 0x10FF4 - 0x0FF4 = NTWK_LN1_ACO_R_DATA
					// 0x10FF8 - 0x0FF8 = NTWK_LN1_ACO_W_ADD
					// 0x10FFC - 0x0FFC = NTWK_LN1_ACO_W_DATA

					if(RecvBuffer[0x01] == 0x32)
					{
						// Set the ShadowRam indirect register bit [16] - will be cleared once read complete
						addr = (Xil_In32(XPAR_BRAM_0_BASEADDR+8) >> 2);
						addr = addr - 0x00010000;
						Xil_Out32(addr, 0x00010000);
					}
				}
			}

			if(RecvBuffer[0x01] == '2')
			{	// read command type
				waitForReply = true;
			}

			/// Unblock the tlp driver
			xTaskNotifyGive( tlpDrv_IncomingMessagesTaskHdl );
		}
		else if(RecvBuffer[0x01] == INDIRECT_MODE_READ_REG_ADDR)
		{	// poll for read register result from shadow ram

		}

		SendBuffer[0x00] = '<';
		SendBuffer[0x01] = 0x01;
		SendBuffer[0x02] = 0x00;
		SendBuffer[0x03] = 16;
		while(waitForReply == true)
		{
			if(Xil_In32(XPAR_BRAM_0_BASEADDR+16) == 0x12345678)
			{	// read the reply from the TLP driver
				Xil_Out32(XPAR_BRAM_0_BASEADDR+16, 0x00000000);	// reset

				data = Xil_In32(XPAR_BRAM_0_BASEADDR);
				SendBuffer[0x04] = (uint8_t)(data >> 24);
				SendBuffer[0x05] = (uint8_t)(data >> 16);
				SendBuffer[0x06] = (uint8_t)(data >> 8);
				SendBuffer[0x07] = (uint8_t)(data >> 0);

				data = Xil_In32(XPAR_BRAM_0_BASEADDR+4);
				SendBuffer[0x08] = (uint8_t)(data >> 24);
				SendBuffer[0x09] = (uint8_t)(data >> 16);
				SendBuffer[10] = (uint8_t)(data >> 8);
				SendBuffer[11] = (uint8_t)(data >> 0);

				data = Xil_In32(XPAR_BRAM_0_BASEADDR+8);
				SendBuffer[12] = (uint8_t)(data >> 24);
				SendBuffer[13] = (uint8_t)(data >> 16);
				SendBuffer[14] = (uint8_t)(data >> 8);
				SendBuffer[15] = (uint8_t)(data >> 0);

				data = Xil_In32(XPAR_BRAM_0_BASEADDR+12);
				SendBuffer[16] = (uint8_t)(data >> 24);
				SendBuffer[17] = (uint8_t)(data >> 16);
				SendBuffer[18] = (uint8_t)(data >> 8);
				SendBuffer[19] = (uint8_t)(data >> 0);

				SendBuffer[20] = '>';

				XUartPs_Send(UartInstPtr, SendBuffer, 20);
				waitForReply = false;
			}
		}
	}
#endif

#if 0
	SendBuffer[0] = 'l';
	SendBuffer[1] = 'j';
	SendBuffer[2] = 'h';
	XUartPs_Send(UartInstPtr, SendBuffer, 3);


	/* uart command decode */
	switch (RecvBuffer[bytcount++]) {
	case '0': /* address only not implemented*/
		break;

	case '2':/* read an mdio address */
		ToggleLed(0);
		/* read in the length */
		cnt = Get16bitdata(&RecvBuffer[bytcount]);
		/* checksum check? */
		if (!RxChecksum(RecvBuffer, 9))
			flt = 'F'; /* indicate a faulty checksum */
		/* set address */
		local_16ut = Get16bitdata(&RecvBuffer[bytcount + 4]);
		/* if none ascii number rxed bail*/
		if (local_16ut == -1)
			flt = 'E'; /* indicate fault */
		mdiRd.strtRaddr = local_16ut;
		mdiRd.rlen = 1;
		cnt = 0;
		/* if valid send MDIO command */
		if (flt == 0) {
			MdioBlkRead(mdiRd);
			SendBuffer[cnt++] = '2'; /* repeat command back */
		} else
		/* indicate fault to host */
		{
			SendBuffer[cnt++] = flt;
		}
		/* number of bytes in reply  */
		SendBuffer[cnt++] = '0';
		SendBuffer[cnt++] = '0';
		SendBuffer[cnt++] = '0';
		SendBuffer[cnt++] = '6';
		/* convert read data to ascii represntation */
		Conv16bToAsc(*mdiRd.rdata, &SendBuffer[cnt]);
		cnt += 4;
		CalcCheck(SendBuffer, cnt);
		/* single read reply always 11 bytes */
		XUartPs_Send(UartInstPtr, SendBuffer, 11);
		break;

	case '1': /* write mdio reg */
		ToggleLed(1);
		/* read the length */
		cnt = Get16bitdata(&RecvBuffer[bytcount]);
		if (cnt > MAX_WR_SIZE)
			break; /* too big to handle */
		/* checksum check */
		if (!RxChecksum(RecvBuffer, cnt + 3))
			break;
		/* set start address */
		local_16ut = Get16bitdata(&RecvBuffer[bytcount + 4]);
		if (local_16ut == -1)
			break;
		bytcount += 8;
		;
		mdiWr.strtwaddr = local_16ut;
		mdiWr.length = (cnt - 6) >> 2;
		cnt = mdiWr.length;
		/* get new data to write */
		uint16_t idx = 0;
		do {
			local_16ut = Get16bitdata(&RecvBuffer[bytcount]);
			if (local_16ut == -1)
				break;
			Wr_Data[idx++] = (uint16_t) local_16ut;
			bytcount += 4;
		} while (--cnt);
		/* give up if a problem  with the rxed data*/
		if (cnt != 0)
			break;

		mdiWr.wdata = &Wr_Data[0];
		/* actually write it to ACO */
		MdioWrite(mdiWr);
		break;

	default:
		ToggleLed(3);
		break;

	}
#endif

	UartPrcess = false;
	XUartPs_Recv(UartInstPtr, RecvBuffer, TEST_BUFFER_SIZE);
}
bool RxChecksum(uint8_t * rxb, uint16_t len)
{
	/* store rxed csum*/
	uint8_t rxedcsum[2];
	rxb+=len;
	rxedcsum[0] = *rxb++;
	rxedcsum[1] = *rxb;
	rxb-=len+1;
	CalcCheck(rxb,len);
	rxb+=len;
	if(rxedcsum[0]==*rxb++)
	{
		if(rxedcsum[1]==*rxb)
			return true;
	}
	return false;
}
void CalcCheck(uint8_t * Buff, uint16_t len)
{
	uint8_t csum = 0;
	while(len--)
	{
		csum +=  *Buff++;
	}
	csum = ~csum;
	csum++;
	*Buff++ = AscCnv((csum & 0xF0)>>4);
	*Buff = AscCnv(csum & 0x0F);

}
void Conv16bToAsc(uint16_t val,uint8_t * sdbuf)
{
	uint16_t mask = 0xF000;
	uint16_t retval=0;
	uint16_t shift = 16;
	do{
		shift-=4;
		*sdbuf++ =(AscCnv((uint8_t)((val & mask )>>shift)));
		mask>>=4;
		}while(shift!=0);

}
uint8_t AscCnv(uint8_t conv)
{
	if(conv<=9)
		return 0x30 + conv;
	else
		return 0x37 + conv;
}
/*

* This function does a minimal test on the UartPS device and driver as a
* design example. The purpose of this function is to illustrate
* how to use the XUartPs driver.
*
* This function sends data and expects to receive the same data through the
* device using the local loopback mode.
*
* This function uses interrupt mode of the device.
*
* @param	IntcInstPtr is a pointer to the instance of the Scu Gic driver.
* @param	UartInstPtr is a pointer to the instance of the UART driver
*		which is going to be connected to the interrupt controller.
* @param	DeviceId is the device Id of the UART device and is typically
*		XPAR_<UARTPS_instance>_DEVICE_ID value from xparameters.h.
* @param	UartIntrId is the interrupt Id and is typically
*		XPAR_<UARTPS_instance>_INTR value from xparameters.h.
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note
*
* This function contains an infinite loop such that if interrupts are not
* working it may never return.
*
**************************************************************************/
int UartInit(INTC *IntcInstPtr, XUartPs *UartInstPtr,
			u16 DeviceId, u16 UartIntrId)
{
	int Status;
	XUartPs_Config *Config;
	u32 IntrMask;

	if (XGetPlatform_Info() == XPLAT_ZYNQ_ULTRA_MP) {
#ifdef XPAR_XUARTPS_1_DEVICE_ID
		DeviceId = XPAR_XUARTPS_1_DEVICE_ID;
#endif
	}

	/*
	 * Initialize the UART driver so that it's ready to use
	 * Look up the configuration in the config table, then initialize it.
	 */
	Config = XUartPs_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XUartPs_CfgInitialize(UartInstPtr, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/* Check hardware build */
	Status = XUartPs_SelfTest(UartInstPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the UART to the interrupt subsystem such that interrupts
	 * can occur. This function is application specific.
	 */
	Status = SetupInterruptSystem(IntcInstPtr, UartInstPtr, UartIntrId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Setup the handlers for the UART that will be called from the
	 * interrupt context when data has been sent and received, specify
	 * a pointer to the UART driver instance as the callback reference
	 * so the handlers are able to access the instance data
	 */
	XUartPs_SetHandler(UartInstPtr, (XUartPs_Handler)Handler, UartInstPtr);

	/*
	 * Enable the interrupt of the UART so interrupts will occur, setup
	 * a local loopback so data that is sent will be received.
	 */
	IntrMask =
		XUARTPS_IXR_TOUT | XUARTPS_IXR_PARITY | XUARTPS_IXR_FRAMING |
		XUARTPS_IXR_OVER | XUARTPS_IXR_TXEMPTY | XUARTPS_IXR_RXFULL |
		XUARTPS_IXR_RXOVR;

	if (UartInstPtr->Platform == XPLAT_ZYNQ_ULTRA_MP) {
		IntrMask |= XUARTPS_IXR_RBRK;
	}

	XUartPs_SetInterruptMask(UartInstPtr, IntrMask);

	XUartPs_SetOperMode(UartInstPtr, XUARTPS_OPER_MODE_NORMAL);

	/*
	 * Set the receiver timeout. If it is not set, and the last few bytes
	 * of data do not trigger the over-water or full interrupt, the bytes
	 * will not be received. By default it is disabled.
	 *
	 * The setting of 8 will timeout after 8 x 4 = 32 character times.
	 * Increase the time out value if baud rate is high, decrease it if
	 * baud rate is low.
	 */
	XUartPs_SetRecvTimeout(UartInstPtr, 8);


	/*
	 * Start receiving data before sending it since there is a loopback,
	 * ignoring the number of bytes received as the return value since we
	 * know it will be zero
	 */
	XUartPs_Recv(UartInstPtr, RecvBuffer, TEST_BUFFER_SIZE);




	return XST_SUCCESS;
}
void UartTask(  XUartPs *UartInstPtr)
{
	if (UartPrcess)
			ProcessUart(UartInstPtr);


}
/* get 16 bit variable from vcp converted from ascii chars */
int32_t Get16bitdata ( uint8_t  * varpr )
{
	uint16_t agg_16bit=0;
	uint16_t digt;
	uint16_t shift = 16;

	do{
		digt = CheckAscDigit(*varpr++);
		/* illegal char */
		if(digt==0xFF)
			return-1;
		shift-=4;
		agg_16bit |= digt<<shift;
	}while(shift>0);

	return (int32_t)agg_16bit;
}
/* check char is an ascii representation of hex digit */
uint8_t CheckAscDigit( uint8_t ascdig )
{
	if((ascdig <=0x39)&&(ascdig >= 0x30))
		return (ascdig - 0x30);
	else if((ascdig>='A') &&(ascdig<='F'))
		return(ascdig-0x37);
	else
		return 0xFF;
}
/**************************************************************************/
/**
*
* This function is the handler which performs processing to handle data events
* from the device.  It is called from an interrupt context. so the amount of
* processing should be minimal.
*
* This handler provides an example of how to handle data for the device and
* is application specific.
*
* @param	CallBackRef contains a callback reference from the driver,
*		in this case it is the instance pointer for the XUartPs driver.
* @param	Event contains the specific kind of event that has occurred.
* @param	EventData contains the number of bytes sent or received for sent
*		and receive events.
*
* @return	None.
*
* @note		None.
*
***************************************************************************/
void Handler(void *CallBackRef, u32 Event, unsigned int EventData)
{
	/* All of the data has been sent */
	if (Event == XUARTPS_EVENT_SENT_DATA) {
		TotalSentCount = EventData;
	}

	/* All of the data has been received */
	if (Event == XUARTPS_EVENT_RECV_DATA) {
		TotalReceivedCount = EventData;
		//UartPrcess = true;
	}

	/*
	 * Data was received, but not the expected number of bytes, a
	 * timeout just indicates the data stopped for 8 character times
	 */
	if (Event == XUARTPS_EVENT_RECV_TOUT) {
		TotalReceivedCount = EventData;
		UartPrcess = true;

	}

	/*
	 * Data was received with an error, keep the data but determine
	 * what kind of errors occurred
	 */
	if (Event == XUARTPS_EVENT_RECV_ERROR) {
		TotalReceivedCount = EventData;
		TotalErrorCount++;
		//UartPrcess = true;
	}

	/*
	 * Data was received with an parity or frame or break error, keep the data
	 * but determine what kind of errors occurred. Specific to Zynq Ultrascale+
	 * MP.
	 */
	if (Event == XUARTPS_EVENT_PARE_FRAME_BRKE) {
		TotalReceivedCount = EventData;
		TotalErrorCount++;
		//UartPrcess = true;
	}

	/*
	 * Data was received with an overrun error, keep the data but determine
	 * what kind of errors occurred. Specific to Zynq Ultrascale+ MP.
	 */
	if (Event == XUARTPS_EVENT_RECV_ORERR) {
		TotalReceivedCount = EventData;
		TotalErrorCount++;
		//UartPrcess = true;
	}
}


/*****************************************************************************/
/**
*
* This function sets up the interrupt system so interrupts can occur for the
* Uart. This function is application-specific. The user should modify this
* function to fit the application.
*
* @param	IntcInstancePtr is a pointer to the instance of the INTC.
* @param	UartInstancePtr contains a pointer to the instance of the UART
*		driver which is going to be connected to the interrupt
*		controller.
* @param	UartIntrId is the interrupt Id and is typically
*		XPAR_<UARTPS_instance>_INTR value from xparameters.h.
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note		None.
*
****************************************************************************/
static int SetupInterruptSystem(INTC *IntcInstancePtr,
				XUartPs *UartInstancePtr,
				u16 UartIntrId)
{
	int Status;

#ifdef XPAR_INTC_0_DEVICE_ID
#ifndef TESTAPP_GEN
	/*
	 * Initialize the interrupt controller driver so that it's ready to
	 * use.
	 */
	Status = XIntc_Initialize(IntcInstancePtr, INTC_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
#endif
	/*
	 * Connect the handler that will be called when an interrupt
	 * for the device occurs, the handler defined above performs the
	 * specific interrupt processing for the device.
	 */
	Status = XIntc_Connect(IntcInstancePtr, UartIntrId,
		(XInterruptHandler) XUartPs_InterruptHandler, UartInstancePtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

#ifndef TESTAPP_GEN
	/*
	 * Start the interrupt controller so interrupts are enabled for all
	 * devices that cause interrupts.
	 */
	Status = XIntc_Start(IntcInstancePtr, XIN_REAL_MODE);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
#endif
	/*
	 * Enable the interrupt for uart
	 */
	XIntc_Enable(IntcInstancePtr, UartIntrId);

	#ifndef TESTAPP_GEN
	/*
	 * Initialize the exception table.
	 */
	Xil_ExceptionInit();

	/*
	 * Register the interrupt controller handler with the exception table.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				(Xil_ExceptionHandler) XIntc_InterruptHandler,
				IntcInstancePtr);
	#endif
#else
#ifndef TESTAPP_GEN
	XScuGic_Config *IntcConfig; /* Config for interrupt controller */

	/* Initialize the interrupt controller driver */
	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == IntcConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
					IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the interrupt controller interrupt handler to the
	 * hardware interrupt handling logic in the processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				(Xil_ExceptionHandler) XScuGic_InterruptHandler,
				IntcInstancePtr);
#endif

	/*
	 * Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler
	 * performs the specific interrupt processing for the device
	 */
	Status = XScuGic_Connect(IntcInstancePtr, UartIntrId,
				  (Xil_ExceptionHandler) XUartPs_InterruptHandler,
				  (void *) UartInstancePtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/* Enable the interrupt for the device */
	XScuGic_Enable(IntcInstancePtr, UartIntrId);

#endif
#ifndef TESTAPP_GEN
	/* Enable interrupts */
	 Xil_ExceptionEnable();
#endif

	return XST_SUCCESS;
}
