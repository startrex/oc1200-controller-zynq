/**
  ****************************************************************************
  * @file    errors.h
  * @author  AGF
  * @version V1.0
  * @date    9 Nov 2012
  * @brief   Error code definitions
  * @section COPYRIGHT (C) Oclaro Technology Plc 2008 - 2014
  ****************************************************************************
  */


/** @addtogroup Utility
  * @{
  */

/** @addtogroup Errors
  * @{
  */


/* Define to prevent recursive inclusion ************************************/
#ifndef ERRORS_H_
#define ERRORS_H_

#include <stdint.h>

/* Public typedef ***********************************************************/

/* Error type */
typedef uint16_t errors_t;

/* Errors */
typedef enum
{
	ERR_NONE					= 0,	/* No error */
	ERR_TIMEOUT 				= 1,	/* Timeout */
	ERR_ABORT					= 2,	/* Procedure aborted */
	ERR_QUIT					= 3,	/* Procedure aborted by request */
	ERR_BUSY					= 4,	/* Device is busy */
	ERR_DIV_BY_ZERO				= 5,	/* Division by zero occurred, or would have */
	ERR_INVALID_ARG				= 6,	/* Invalid argument */
	ERR_WRONG_ARG_COUNT			= 7,	/* Wrong number of arguments */
	ERR_OVERFLOW				= 8,	/* Overflow */
	ERR_INITIALISE_FAILED		= 9,	/* Device or function initialisation failed */
	ERR_NOT_INITIALISED			= 10,	/* Device or function not initialised */
	ERR_NOT_IMPLEMENTED			= 11,	/* Device or function not implemented */
	ERR_OPERATION_FAILED		= 12,	/* Operation failed */
	ERR_OPERATION_NOT_VALID		= 13,	/* Operation not valid, e.g. write to RO register */
	ERR_OPERATION_NOT_ALLOWED	= 14,	/* Operation not allowed, e.g. access level too low */
	ERR_TEC_FAILURE				= 15,	/* TEC fault */
	ERR_INVALID_DEVICE			= 16,	/* Invalid device */
	ERR_INVALID_CHANNEL			= 17,	/* Invalid channel */
	ERR_CHECKSUM_FAIL			= 18,	/* Checksum not matched */
	ERR_READ_ONLY				= 19,	/* Parameter is not writeable */
	ERR_UNKNOWN					= 255	/* Unknown error */
} ERRORS;


/**
 * ASSERT a return on error.
 * Useful macro for use in functions that return an error value.
 *
 * If the function foo() returns an error then the ASSERT() macro will cause the
 * calling function to return the error if the error is not ERR_NONE, otherwise
 * no action is taken.
 *
 * Instead of writing this:
 *
 *     error err;
 *     ...
 *     err = foo();
 *	   if(err != ERR_NONE) {
 *          return err;
 *     }
 *
 *  write this instead:
 *
 *     ASSERT(foo());
 *
 */
#define ASSERT(x)	{ error _err; if( (_err=(x))!=ERR_NONE ) return _err; }


#endif /* ERRORS_H_ */


/*@} end Errors */
/*@} end Utility */
/******************************* END OF FILE ********************************/
