/************************** Function Prototypes *****************************/
#include "xscugic.h"
#include "xplatform_info.h"
#include "xparameters.h"
#include "errors.h"
#include "xuartps.h"

#define UARTRX_PRIORITY tskIDLE_PRIORITY +1
/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#ifdef XPAR_INTC_0_DEVICE_ID
#define INTC		XIntc
#define UART_DEVICE_ID		XPAR_XUARTPS_0_DEVICE_ID
#define INTC_DEVICE_ID		XPAR_INTC_0_DEVICE_ID
#define UART_INT_IRQ_ID		XPAR_INTC_0_UARTPS_0_VEC_ID
#else
#define INTC		XScuGic
#define UART_DEVICE_ID		XPAR_XUARTPS_0_DEVICE_ID
#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID
#define UART_INT_IRQ_ID		XPAR_XUARTPS_1_INTR
#endif
#define FABRIC_BASE_ADDRESS                   0x43C00000
#define STD_BASE_ADDRESS                      FABRIC_BASE_ADDRESS
#define HIF_BASE_ADDRESS                      FABRIC_BASE_ADDRESS + 0x1000
#define SER_BASE_ADDRESS                      FABRIC_BASE_ADDRESS + 0x2000
#define PCI_BASE_ADDRESS                      FABRIC_BASE_ADDRESS + 0x3000

#define STD_SCRATCH_ADDR0                     STD_BASE_ADDRESS
#define STD_SCRATCH_ADDR1                     STD_BASE_ADDRESS + 0x4
#define STD_DEBUG_ADDR                        STD_BASE_ADDRESS + 0x8
#define STD_FPGA_REV_ADDR                     STD_BASE_ADDRESS + 0x10
#define STD_SW_RESET_INTERNAL_ADDR            STD_BASE_ADDRESS + 0x20
#define STD_SW_RESET_EXTERNAL_ADDR            STD_BASE_ADDRESS + 0x24
#define STD_IRQ_STAT_ADDR                     STD_BASE_ADDRESS + 0x30

#define SER_SCRATCH_ADDR0                     SER_BASE_ADDRESS
#define SER_SCRATCH_ADDR1                     SER_BASE_ADDRESS + 0x4
#define SER_SERIAL_IRQ_ADDR                   SER_BASE_ADDRESS + 0x14
#define SER_MDIO_MAST_0_CTRL_DATA_ADDR        SER_BASE_ADDRESS + 0x100
#define SER_MDIO_MAST_0_WRITE_DATA_ADDR       SER_BASE_ADDRESS + 0x104
#define SER_MDIO_MAST_0_READ_DATA_ADDR        SER_BASE_ADDRESS + 0x108
#define SER_MDIO_MAST_0_PREAMBLE_ADDR         SER_BASE_ADDRESS + 0x10C
#define SER_MDIO_MAST_0_STATUS_ADDR           SER_BASE_ADDRESS + 0x110
#define SER_MDIO_MAST_0_CONFIG_ADDR           SER_BASE_ADDRESS + 0x114
#define SER_MDIO_MAST_1_CTRL_DATA_ADDR        SER_BASE_ADDRESS + 0x120
#define SER_MDIO_MAST_1_WRITE_DATA_ADDR       SER_BASE_ADDRESS + 0x124
#define SER_MDIO_MAST_1_READ_DATA_ADDR        SER_BASE_ADDRESS + 0x128
#define SER_MDIO_MAST_1_PREAMBLE_ADDR         SER_BASE_ADDRESS + 0x12C
#define SER_MDIO_MAST_1_STATUS_ADDR           SER_BASE_ADDRESS + 0x130
#define SER_MDIO_MAST_1_CONFIG_ADDR           SER_BASE_ADDRESS + 0x134
#define SER_MDIO_SLAVE_0_CTRL_DATA_ADDR       SER_BASE_ADDRESS + 0x140
#define SER_MDIO_SLAVE_0_DATA_TO_MASTER_ADDR  SER_BASE_ADDRESS + 0x144
#define SER_MDIO_SLAVE_0_DATA_FRM_MASTER_ADDR SER_BASE_ADDRESS + 0x148
#define SER_MDIO_SLAVE_0_ADDR_FRM_MASTER_ADDR SER_BASE_ADDRESS + 0x14C
int UartInit(INTC *IntcInstPtr, XUartPs *UartInstPtr,
			u16 DeviceId, u16 UartIntrId);


static int SetupInterruptSystem(INTC *IntcInstancePtr,
				XUartPs *UartInstancePtr,
				u16 UartIntrId);

void Handler(void *CallBackRef, u32 Event, unsigned int EventData);
void ProcessUart ( XUartPs *UartInstPtr );
int32_t Get16bitdata ( uint8_t  * varpr );
uint8_t CheckAscDigit( uint8_t ascdig );
void ToggleLed(uint16_t led);
void UartTask(  XUartPs *UartInstPtr);
errors_t InitUart( void );
