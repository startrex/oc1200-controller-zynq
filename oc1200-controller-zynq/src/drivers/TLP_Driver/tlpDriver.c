/**
****************************************************************************
* @file tlpDriver.h
* @author rex
* @version 0.1
* @date 28 February 2018
* @brief TLP Driver Implementation
* @section COPYRIGHT (C) Oclaro 2008 - 2018
*
* A more complete description goes here
****************************************************************************
*/
/** @addtogroup Drivers
* @{
*/
/** @addtogroup TLP_Driver
* @{
*/
/* Includes *****************************************************************/
/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "xil_io.h"
#include "xil_printf.h"

#include "stdbool.h"
#include "../../utils/log.h"

#include "tlpDriver.h"
#include "../MDIO_Driver/MDIO_Driver.h"

/* Defines ******************************************************************/
#define DELAY_10_SECONDS	1000UL

/* Private Structures go here ***********************************************/
struct tlpWritePacket
{
	uint32_t dw0;
	uint32_t dw1;
	uint32_t dw2;
	uint32_t dw3;
};

/* Private Variables ********************************************************/
/// Task handler for the outgoing TLP messages task
TaskHandle_t tlpDrv_OutgoingTlpMessagesTaskHdl = NULL;
/// Test only
TaskHandle_t tlpDrv_MockIncomingTlpInteruptTaskHdl = NULL;

/// Mock a memory location to read in a TLP message from the PP
struct tlpWritePacket gTlpInPacket;

/* Private Function Prototypes **********************************************/
void tlpDrv_IncomingTlpMessagesTask(void * arg);
void tlpDrv_OutgoingTlpMessagesTask(void * arg);
void tlpDrv_MockIncomingTlpInteruptTask(void * arg);

/**
 * @brief Function to aid in test of the tlpDrv_ParseIncomingTlpMessage() function
 */
void tlpDrv_test_ParseIncomingTlpMessage(uint32_t blk1,
						uint32_t blk2,
						uint32_t blk3,
						uint32_t blk4,
						struct ATlpDrvQueueItem *tlpQueueItem)
{
#if 0
	memcpy((uint32_t*)gTlpInPacket.dw0, &blk1, sizeof(uint32_t));
	memcpy((uint32_t*)gTlpInPacket.dw1, &blk2, sizeof(uint32_t));
	memcpy((uint32_t*)gTlpInPacket.dw2, &blk3, sizeof(uint32_t));
	memcpy((uint32_t*)gTlpInPacket.dw3, &blk4, sizeof(uint32_t));

	/// Parse the incoming TLP message
	tlpDrv_ParseIncomingTlpMessage(&tlpQueueItem);
#endif
}

void tlpDrv_ParseIncomingTlpMessage(struct ATlpDrvQueueItem *tlpQueueItem);

/**
 * @brief Function that parses the incoming TLP messages
 *
 * TODO - Complete this description
 * @param TODO
 * @return TODO
 */
void tlpDrv_ParseIncomingTlpMessage(struct ATlpDrvQueueItem *tlpQueueItem)
{
	tlpQueueItem->fmt = (uint8_t)((uint32_t)((TLP_FMT_MASK & gTlpInPacket.dw0) >> TLP_FMT_LSHIFT));
	tlpQueueItem->type = (uint8_t)((uint32_t)((TLP_TYPE_MASK & gTlpInPacket.dw0) >> TLP_TYPE_LSHIFT));

	if(tlpQueueItem->fmt == 0x02 && tlpQueueItem->type == 0x00)
	{	// Memory write request TLP
		tlpQueueItem->tc = (uint8_t)((uint32_t)((TLP_TC_MASK & gTlpInPacket.dw0) >> TLP_TC_LSHIFT));
		tlpQueueItem->td = (uint8_t)((uint32_t)((TLP_TD_MASK & gTlpInPacket.dw0) >> TLP_TD_LSHIFT));
		tlpQueueItem->ep = (uint8_t)((uint32_t)((TLP_EP_MASK & gTlpInPacket.dw0) >> TLP_EP_LSHIFT));
		tlpQueueItem->attr = (uint8_t)((uint32_t)((TLP_ATTR_MASK & gTlpInPacket.dw0) >> TLP_ATTR_LSHIFT));
		tlpQueueItem->length = (uint16_t)((uint32_t)((TLP_LENGTH_MASK & gTlpInPacket.dw0) >> TLP_LENGTH_LSHIFT));

		tlpQueueItem->requesterId = (uint16_t)((uint32_t)((TLP_REQUESTER_ID_MASK & gTlpInPacket.dw1) >> TLP_REQUESTER_ID_LSHIFT));
		tlpQueueItem->tag = (uint8_t)((uint32_t)((TLP_TAG_MASK & gTlpInPacket.dw1) >> TLP_TAG_LSHIFT));
		tlpQueueItem->lastBE = (uint8_t)((uint32_t)((TLP_LAST_BE_MASK & gTlpInPacket.dw1) >> TLP_LAST_BE_LSHIFT));
		tlpQueueItem->firstBE = (uint8_t)((uint32_t)((TLP_FIRST_BE_MASK & gTlpInPacket.dw1) >> TLP_FIRST_BE_LSHIFT));

		tlpQueueItem->addr = ((uint32_t)((TLP_ADDRESS_MASK & gTlpInPacket.dw2) >> TLP_ADDRESS_LSHIFT));
		tlpQueueItem->data = ((uint32_t)((TLP_DATA_MASK & gTlpInPacket.dw3) >> TLP_DATA_LSHIFT));
	}
	else if(tlpQueueItem->fmt == 0x00 && tlpQueueItem->type == 0x00)
	{	// Memory read request TLP
		tlpQueueItem->tc = (uint8_t)((uint32_t)((TLP_TC_MASK & gTlpInPacket.dw0) >> TLP_TC_LSHIFT));
		tlpQueueItem->td = (uint8_t)((uint32_t)((TLP_TD_MASK & gTlpInPacket.dw0) >> TLP_TD_LSHIFT));
		tlpQueueItem->ep = (uint8_t)((uint32_t)((TLP_EP_MASK & gTlpInPacket.dw0) >> TLP_EP_LSHIFT));
		tlpQueueItem->attr = (uint8_t)((uint32_t)((TLP_ATTR_MASK & gTlpInPacket.dw0) >> TLP_ATTR_LSHIFT));
		tlpQueueItem->length = (uint16_t)((uint32_t)((TLP_LENGTH_MASK & gTlpInPacket.dw0) >> TLP_LENGTH_LSHIFT));

		tlpQueueItem->requesterId = (uint16_t)((uint32_t)((TLP_REQUESTER_ID_MASK & gTlpInPacket.dw1) >> TLP_REQUESTER_ID_LSHIFT));
		tlpQueueItem->tag = (uint8_t)((uint32_t)((TLP_TAG_MASK & gTlpInPacket.dw1) >> TLP_TAG_LSHIFT));
		tlpQueueItem->lastBE = (uint8_t)((uint32_t)((TLP_LAST_BE_MASK & gTlpInPacket.dw1) >> TLP_LAST_BE_LSHIFT));
		tlpQueueItem->firstBE = (uint8_t)((uint32_t)((TLP_FIRST_BE_MASK & gTlpInPacket.dw1) >> TLP_FIRST_BE_LSHIFT));

		tlpQueueItem->addr = ((uint32_t)((TLP_ADDRESS_MASK & gTlpInPacket.dw2) >> TLP_ADDRESS_LSHIFT));
	}
	else if(tlpQueueItem->fmt == 0x02 && tlpQueueItem->type == 0x0A)
	{	// Memory completion request TLP
		tlpQueueItem->tc = (uint8_t)((uint32_t)((TLP_TC_MASK & gTlpInPacket.dw0) >> TLP_TC_LSHIFT));
		tlpQueueItem->td = (uint8_t)((uint32_t)((TLP_TD_MASK & gTlpInPacket.dw0) >> TLP_TD_LSHIFT));
		tlpQueueItem->ep = (uint8_t)((uint32_t)((TLP_EP_MASK & gTlpInPacket.dw0) >> TLP_EP_LSHIFT));
		tlpQueueItem->attr = (uint8_t)((uint32_t)((TLP_ATTR_MASK & gTlpInPacket.dw0) >> TLP_ATTR_LSHIFT));
		tlpQueueItem->length = (uint16_t)((uint32_t)((TLP_LENGTH_MASK & gTlpInPacket.dw0) >> TLP_LENGTH_LSHIFT));

		tlpQueueItem->completerId = (uint16_t)((uint32_t)((TLP_COMPLETER_ID_MASK & gTlpInPacket.dw1) >> TLP_COMPLETER_ID_LSHIFT));
		tlpQueueItem->status = (uint8_t)((uint32_t)((TLP_STATUS_MASK & gTlpInPacket.dw1) >> TLP_STATUS_LSHIFT));
		tlpQueueItem->bcm = (uint8_t)((uint32_t)((TLP_BCM_MASK & gTlpInPacket.dw1) >> TLP_BCM_LSHIFT));
		tlpQueueItem->byteCount = (uint16_t)((uint32_t)((TLP_BYTE_COUNT_MASK & gTlpInPacket.dw1) >> TLP_BYTE_COUNT_LSHIFT));

		tlpQueueItem->requesterId = (uint16_t)((uint32_t)((TLP_REQUESTER_ID_MASK & gTlpInPacket.dw2) >> TLP_REQUESTER_ID_LSHIFT));
		tlpQueueItem->tag = (uint8_t)((uint32_t)((TLP_TAG_MASK & gTlpInPacket.dw2) >> TLP_TAG_LSHIFT));
		tlpQueueItem->lowerAddr = (uint8_t)((uint32_t)((TLP_LOWER_ADDRESS_MASK & gTlpInPacket.dw2) >> TLP_LOWER_ADDRESS_LSHIFT));

		tlpQueueItem->data = ((uint32_t)((TLP_DATA_MASK & gTlpInPacket.dw3) >> TLP_DATA_LSHIFT));
	}
	else
	{
		/// TODO trap error case
		//LOG_INFO("#### UNKNOWN TLP message\r\n");
	}
}

/**
 * @brief Create the task to handle incoming TLP message - in from the PP
 *
 * This task will block on task events and is unblocked by by interrupt driven from the PP
 * @param Task argument
 * @return none
 */
void tlpDrv_IncomingTlpMessagesTask(void * arg)
{
	//struct tlpInboundPacket *pTlpDrvInboundItem = NULL;
	struct ATlpDrvQueueItem tlpQueueItem;
	struct ATlpDrvQueueItem *pTlpOutboundQueueItem = NULL;

	LOG_INFO("T L P   D R I V E R   I N C O M I N G   M E S S A G E S  T A S K  R U N N I N G\r\n");

	//Endless loop
	for( ;; )
    {
		/* Block to wait for interrupt from IST to notify this task. */
		ulTaskNotifyTake( pdTRUE, portMAX_DELAY );

		//LOG_INFO("tlpDrv_IncomingTlpMessagesTask - TLP message available - processing ");

		gTlpInPacket.dw0 = Xil_In32(XPAR_BRAM_0_BASEADDR);
		gTlpInPacket.dw1 = Xil_In32(XPAR_BRAM_0_BASEADDR+4);
		gTlpInPacket.dw2 = Xil_In32(XPAR_BRAM_0_BASEADDR+8);
		gTlpInPacket.dw3 = Xil_In32(XPAR_BRAM_0_BASEADDR+12);

		/// Parse the incoming TLP message
		tlpDrv_ParseIncomingTlpMessage(&tlpQueueItem);

		/*LOG_INFO("tlpDrv_IncomingTlpMessagesTask take\r\n");

		LOG_INFO("#### TLP message popped off queue\r\n");

		LOG_INFO("#### dw0 = 0x%02x\r\n", gTlpInPacket.dw0);
		LOG_INFO("#### dw1 = 0x%02x\r\n", gTlpInPacket.dw1);
		LOG_INFO("#### dw2 = 0x%02x\r\n", gTlpInPacket.dw2);
		LOG_INFO("#### dw3 = 0x%02x\r\n", gTlpInPacket.dw3);*/

		pTlpOutboundQueueItem = &tlpQueueItem;
		if(xQueueSendToBack( xMdioInboundQueue, ( void * ) &pTlpOutboundQueueItem, ( TickType_t ) 0 ) == 0x00)
		{
			//LOG_INFO(">>> ERROR in pushing to  xTlpDrvQueue\r\n");
		}
		//LOG_INFO("tlpDrv_IncomingTlpMessagesTask - TLP queue object pushed onto queue");

    }
}

/**
 * @brief Create the task to generate outgoing TLP message - out to the PP
 *
 * This task will block on incoming queue which will be unblocked on a new message object\n
 * being pushed onto the queue
 * @param Task argument
 * @return none
 */
void tlpDrv_OutgoingTlpMessagesTask(void * arg)
{
	struct ATlpDrvQueueItem* pTlpDrvInboundQueueItem;
	uint32_t data = 0x00;

	LOG_INFO("T L P   D R I V E R   O U T G O I N G   M E S S A G E S  T A S K  R U N N I N G\r\n");

	// init the mdio queue
	xTlpInboundQueue = xQueueCreate( 10, sizeof( struct ATlpDrvQueueItem * ) );
	if(xTlpInboundQueue == NULL)
	{	// ERROR case
		LOG_INFO("ERROR - Failed to create xTlpInboundQueue\r\n");
		return;
	}

	//Endless loop
	for( ;; )
    {
		if(xQueueReceive( xTlpInboundQueue, &(pTlpDrvInboundQueueItem), (TickType_t) portMAX_DELAY))
		{
			/// build completion packet
			// build dw0
			data = (pTlpDrvInboundQueueItem->fmt << TLP_FMT_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->type << TLP_TYPE_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->tc << TLP_TC_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->td << TLP_TD_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->ep << TLP_EP_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->attr << TLP_ATTR_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->length << TLP_LENGTH_LSHIFT);
			Xil_Out32(XPAR_BRAM_0_BASEADDR+0, data);

			// build dw1
			data = (pTlpDrvInboundQueueItem->completerId << TLP_COMPLETER_ID_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->status << TLP_STATUS_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->bcm << TLP_BCM_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->byteCount << TLP_BYTE_COUNT_LSHIFT);
			Xil_Out32(XPAR_BRAM_0_BASEADDR+4, data);

			// build dw2
			data = (pTlpDrvInboundQueueItem->requesterId << TLP_REQUESTER_ID_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->tag << TLP_TAG_LSHIFT);
			data = data + (pTlpDrvInboundQueueItem->lowerAddr << TLP_LOWER_ADDRESS_LSHIFT);
			Xil_Out32(XPAR_BRAM_0_BASEADDR+8, data);

			// build dw3
			data = (pTlpDrvInboundQueueItem->data << TLP_DATA_LSHIFT);
			Xil_Out32(XPAR_BRAM_0_BASEADDR+12, data);

			Xil_Out32(XPAR_BRAM_0_BASEADDR+16, 0x12345678);		// informs uart driver new data available
#if 0
			LOG_INFO("tlpTask - message popped off xTlpInboundQueue\r\n");

			LOG_INFO("#### fmt = 0x%02x\r\n", pTlpDrvInboundQueueItem->fmt);
			LOG_INFO("#### type = 0x%02x\r\n", pTlpDrvInboundQueueItem->type);
			LOG_INFO("#### tc = 0x%02x\r\n", pTlpDrvInboundQueueItem->tc);
			LOG_INFO("#### td = 0x%02x\r\n", pTlpDrvInboundQueueItem->td);
			LOG_INFO("#### ep = 0x%02x\r\n", pTlpDrvInboundQueueItem->ep);
			LOG_INFO("#### attr = 0x%02x\r\n", pTlpDrvInboundQueueItem->attr);
			LOG_INFO("#### length = 0x%02x\r\n", pTlpDrvInboundQueueItem->length);

			LOG_INFO("#### requesterId = 0x%02x\r\n", pTlpDrvInboundQueueItem->requesterId);
			LOG_INFO("#### completerId = 0x%02x\r\n", pTlpDrvInboundQueueItem->completerId);
			LOG_INFO("#### status = 0x%02x\r\n", pTlpDrvInboundQueueItem->status);
			LOG_INFO("#### bcm = 0x%02x\r\n", pTlpDrvInboundQueueItem->bcm);
			LOG_INFO("#### byteCount = 0x%02x\r\n", pTlpDrvInboundQueueItem->byteCount);
			LOG_INFO("#### tag = 0x%02x\r\n", pTlpDrvInboundQueueItem->tag);
			LOG_INFO("#### lastBE = 0x%02x\r\n", pTlpDrvInboundQueueItem->lastBE);
			LOG_INFO("#### firstBE = 0x%02x\r\n", pTlpDrvInboundQueueItem->firstBE);
			LOG_INFO("#### lowerAddr = 0x%02x\r\n", pTlpDrvInboundQueueItem->lowerAddr);

			LOG_INFO("#### addr = 0x%02x\r\n", pTlpDrvInboundQueueItem->addr);
			LOG_INFO("#### data = 0x%02x\r\n", pTlpDrvInboundQueueItem->data);
#endif
		}
    }
}
#if 0
/**
 * @brief Create a TEST ONLY task to generate mock the PP interrupt
 *
 * @param Task argument
 * @return none
 */
void tlpDrv_MockIncomingTlpInteruptTask(void * arg)
{
	LOG_INFO("T L P   D R I V E R   TEST TASK RUNNING\r\n");

	//Endless loop
	for( ;; )
    {
		vTaskDelay( DELAY_10_SECONDS );

		/// build a dummy tlp packet to mock an incoming TLP packet
		gTlpInPacket.dw0 = 0x40000001;
		gTlpInPacket.dw1 = 0x0000000F;
		gTlpInPacket.dw2 = 0xFDAFF040;
		gTlpInPacket.dw3 = 0x12345678;

		/// Send a notification to tlpDrv_IncomingTlpMessagesTask(),
		/// to mock the PP interrupt
		/// bringing it out of the Blocked state
		xTaskNotifyGive( tlpDrv_IncomingMessagesTaskHdl );

		LOG_INFO("\r\ntlpDrv Mocked TLP message and notified tlpDrv_IncomingMessagesTaskHdl\r\n");
    }
}
#endif

/**
 * \brief Create the TLP Driver
 *
 * Create a task to handle the incoming TLP message from the PP\n
 * Create a task to generate outgoing TLP message\n
 * @return bool - true = successful
 */
bool tlpDrv_Create()
{
	bool retval = true;

	/// Create the task which handles incoming TLP messages from the PP
	xTaskCreate( tlpDrv_OutgoingTlpMessagesTask,
			 ( const char * ) "tlpDrv_OutgoingTlpMessagesTask",
			 configMINIMAL_STACK_SIZE,
			 NULL,
			 tskIDLE_PRIORITY + 3,
			 &tlpDrv_OutgoingTlpMessagesTaskHdl );

	/// Create the task which handles incoming TLP messages from the PP
	xTaskCreate( tlpDrv_IncomingTlpMessagesTask,
			 ( const char * ) "tlpDrv_IncomingTlpMessagesTask",
			 configMINIMAL_STACK_SIZE,
			 NULL,
			 tskIDLE_PRIORITY + 3,
			 &tlpDrv_IncomingMessagesTaskHdl );
#if 0
	/// TEST ONLY - Creating a third task for testing purposes to unblock the tlpDrv_IncomingTlpMessagesTask
	xTaskCreate( tlpDrv_MockIncomingTlpInteruptTask,
			 ( const char * ) "tlpDrv_MockIncomingTlpInteruptTask",
			 configMINIMAL_STACK_SIZE,
			 NULL,
			 tskIDLE_PRIORITY + 3,
			 &tlpDrv_MockIncomingTlpInteruptTaskHdl );
#endif

	return retval;
}

//@} end <module_name>/
//@} end <module_group>/
/******************************* END OF FILE ********************************/

#if 0

/*
 * tlpDriver.c
 *
 *  Created on: 22 Feb 2018
 *      Author: Rex.Taylor
 */

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "stdbool.h"
#include "../../log.h"

#include "tlpDriver.h"

// TLP packet masks
#define TLP_FMT_MASK			0x60000000		// DW 0 [30:29]
#define TLP_TYPE_MASK			0x1F000000		// DW 0 [24:28]
#define TLP_TC_MASK				0x00700000		// DW 0 [19:16]
#define TLP_TD_MASK				0x00008000		// DW 0 [15]
#define TLP_EP_MASK				0x00004000		// DW 0 [14]
#define TLP_ATTR_MASK			0x00003000		// DW 0 [14]
#define TLP_LENGTH_MASK			0x000003FF		// DW 0 [9:0]

#define TLP_REQUESTER_ID_MASK	0xFFFF0000		// DW 1 [31:16]
#define TLP_TAG_MASK			0x0000FF00		// DW 1 or DW 2 [15:8]
#define TLP_LAST_BE_MASK		0x000000F0		// DW 1 [7:4]
#define TLP_FIRST_BE_MASK		0x0000000F		// DW 1 [3:0]

#define TLP_ADDRESS_MASK		0xFFFFFFFC		// DW 2 [31:2]
#define TLP_DATA_MASK			0xFFFFFFFF		// DW 3 [31:0]

#define TLP_COMPLETER_ID_MASK	0xFFFF0000		// DW 1 [31:16]	- used by completion message
#define TLP_STATUS_MASK			0x0000E000		// DW 1 [15:13]
#define TLP_BCM_MASK			0x00001000		// DW 1 [12]
#define TLP_BYTE_COUNT_MASK 	0x00000FFF		// DW 1 [11:0]
#define TLP_LOWER_ADDRESS_MASK	0x0000007F		// DW 2 [6:0]

#define TLP_FMT_LSHIFT			29UL
#define TLP_TYPE_LSHIFT			24UL
#define TLP_TC_LSHIFT			20UL
#define TLP_TD_LSHIFT			15UL
#define TLP_EP_LSHIFT			14UL
#define TLP_ATTR_LSHIFT			12UL
#define TLP_LENGTH_LSHIFT		0UL

#define TLP_REQUESTER_ID_LSHIFT	16UL
#define TLP_TAG_LSHIFT			8UL
#define TLP_LAST_BE_LSHIFT		4UL
#define TLP_FIRST_BE_LSHIFT		0UL

#define TLP_ADDRESS_LSHIFT		2UL
#define TLP_DATA_LSHIFT			0UL

#define TLP_COMPLETER_ID_LSHIFT	16UL
#define TLP_STATUS_LSHIFT		13UL
#define TLP_BCM_LSHIFT			12UL
#define TLP_BYTE_COUNT_LSHIFT 	0UL
#define TLP_LOWER_ADDRESS_LSHIFT	0UL


#define DELAY_10_SECONDS	10000UL

TaskHandle_t tlpDrvTaskHdl = NULL;
TaskHandle_t tlpDrvTestTaskHdl = NULL;

static QueueHandle_t xTlpDrvQueue = NULL;

bool tlpDrv_EngineInit()
{
	xTaskCreate( tlpDrvIncomingMessagesTask,
		 ( const char * ) "tlpDrvIncomingMessages",
		 configMINIMAL_STACK_SIZE,
		 NULL,
		 tskIDLE_PRIORITY + 3,
		 &tlpDrvTaskHdl );

	xTaskCreate( tlpDrvOutgoingMessagesTask,
			 ( const char * ) "tlpDrvOutgoingMessages",
			 configMINIMAL_STACK_SIZE,
			 NULL,
			 tskIDLE_PRIORITY + 3,
			 &tlpDrvTaskHdl );

	// test task, for dev purposes only
	xTaskCreate( tlpDrvTestTask,
		 ( const char * ) "tlpDrvTestTask",
		 configMINIMAL_STACK_SIZE,
		 NULL,
		 tskIDLE_PRIORITY + 3,
		 &tlpDrvTestTaskHdl );

	return true;
}

void tlpDrvIncomingMessagesTask(void * arg)
{
	struct tlpInboundPacket *pTlpDrvInboundItem = NULL;
	struct ATlpDrvQueueItem tlpQueueItem;

	LOG_INFO("T L P   D R I V E R   I N C O M I N G   M E S S A G E S  T A S K  R U N N I N G\r\n");

	//Endless loop
	for( ;; )
    {
		if(xQueueReceive( xTlpDrvQueue, &(pTlpDrvInboundItem), (TickType_t) portMAX_DELAY))
		{

		}
    }
}

void tlpDrvOutgoingMessagesTask(void * arg)
{
	struct tlpInboundPacket *pTlpDrvInboundItem = NULL;
	struct ATlpDrvQueueItem tlpQueueItem;

	LOG_INFO("T L P   D R I V E R   O U T C O M I N G   M E S S A G E S  T A S K  R U N N I N G\r\n");

	xTlpDrvQueue = xQueueCreate( 10, sizeof( struct tlpInboundPacket * ) );
	if(xTlpDrvQueue == NULL)
	{	// ERROR case
		LOG_INFO("ERROR - Failed to create xTlpDrvQueue\r\n");
		return;
	}


	//Endless loop
	for( ;; )
    {
		if(xQueueReceive( xTlpDrvQueue, &(pTlpDrvInboundItem), (TickType_t) portMAX_DELAY))
		{

		}
    }
}

void tlpDrvTestTask(void * arg)
{
	const TickType_t x10seconds = pdMS_TO_TICKS( DELAY_10_SECONDS/10 );

	static uint8_t state = 0x00;

	struct tlpWritePacket tlpDrvOutboundItem;
	struct ATlpDrvQueueItem *pTlpDrvOutboundItem = NULL;

	LOG_INFO("T L P   D R I V E R   T E S T   T A S K  R U N N I N G\r\n");

	//Endless loop
	for( ;; )
    {
		vTaskDelay( x10seconds );

		LOG_INFO("#### Generating TLP message\r\n");

		switch(state)
		{
		case 0x00:		// send write tlp
			tlpDrvOutboundItem.dw0 = 0x40000001;
			tlpDrvOutboundItem.dw1 = 0x0000000F;
			tlpDrvOutboundItem.dw2 = 0xFDAFF040;
			tlpDrvOutboundItem.dw3 = 0x12345678;
			state++;
			break;
		case 0x01:		// send read tlp
			tlpDrvOutboundItem.dw0 = 0x00000001;
			tlpDrvOutboundItem.dw1 = 0x00000C0F;
			tlpDrvOutboundItem.dw2 = 0xFDAFF040;

			state++;
			break;
		case 0x02:		// send completion tlp
			tlpDrvOutboundItem.dw0 = 0x4a000001;
			tlpDrvOutboundItem.dw1 = 0x01000004;
			tlpDrvOutboundItem.dw2 = 0x00000C40;
			tlpDrvOutboundItem.dw3 = 0x12345678;

			state = 0x00;
			break;
		}



		pTlpDrvOutboundItem = &tlpDrvOutboundItem;
		if(xQueueSendToBack( xTlpDrvQueue, ( void * ) &pTlpDrvOutboundItem, ( TickType_t ) 0 ) == 0x00)
		{
			LOG_INFO(">>> ERROR in pushing to  xTlpDrvQueue\r\n");
		}
    }
}

#if 0
void tlpDrvIncomingMessagesTask(void * arg)
{
	struct tlpInboundPacket *pTlpDrvInboundItem = NULL;
	struct ATlpDrvQueueItem tlpQueueItem;

	LOG_INFO("T L P   D R I V E R   T A S K  R U N N I N G\r\n");

	xTlpDrvQueue = xQueueCreate( 10, sizeof( struct tlpInboundPacket * ) );
	if(xTlpDrvQueue == NULL)
	{	// ERROR case
		LOG_INFO("ERROR - Failed to create xTlpDrvQueue\r\n");
		return;
	}


	//Endless loop
	for( ;; )
    {
		if(xQueueReceive( xTlpDrvQueue, &(pTlpDrvInboundItem), (TickType_t) portMAX_DELAY))
		{
			LOG_INFO("#### TLP message popped off queue\r\n");

			LOG_INFO("#### dw0 = 0x%02x\r\n", pTlpDrvInboundItem->dw0);
			LOG_INFO("#### dw1 = 0x%02x\r\n", pTlpDrvInboundItem->dw1);
			LOG_INFO("#### dw2 = 0x%02x\r\n", pTlpDrvInboundItem->dw2);
			LOG_INFO("#### dw3 = 0x%02x\r\n", pTlpDrvInboundItem->dw3);


			memset(&tlpQueueItem, 0x00, sizeof(struct ATlpDrvQueueItem));

			tlpQueueItem.fmt = (uint8_t)((uint32_t)((TLP_FMT_MASK & pTlpDrvInboundItem->dw0) >> TLP_FMT_LSHIFT));
			tlpQueueItem.type = (uint8_t)((uint32_t)((TLP_TYPE_MASK & pTlpDrvInboundItem->dw0) >> TLP_TYPE_LSHIFT));
			LOG_INFO("#### fmt = 0x%02x\r\n", tlpQueueItem.fmt);
			LOG_INFO("#### type = 0x%02x\r\n", tlpQueueItem.type);

			if(tlpQueueItem.fmt == 0x02 && tlpQueueItem.type == 0x00)
			{	// Memory write request TLP
				LOG_INFO("#### Memory write request TLP\r\n");
				tlpQueueItem.tc = (uint8_t)((uint32_t)((TLP_TC_MASK & pTlpDrvInboundItem->dw0) >> TLP_TC_LSHIFT));
				tlpQueueItem.td = (uint8_t)((uint32_t)((TLP_TD_MASK & pTlpDrvInboundItem->dw0) >> TLP_TD_LSHIFT));
				tlpQueueItem.ep = (uint8_t)((uint32_t)((TLP_EP_MASK & pTlpDrvInboundItem->dw0) >> TLP_EP_LSHIFT));
				tlpQueueItem.attr = (uint8_t)((uint32_t)((TLP_ATTR_MASK & pTlpDrvInboundItem->dw0) >> TLP_ATTR_LSHIFT));
				tlpQueueItem.length = (uint16_t)((uint32_t)((TLP_LENGTH_MASK & pTlpDrvInboundItem->dw0) >> TLP_LENGTH_LSHIFT));

				tlpQueueItem.requesterId = (uint16_t)((uint32_t)((TLP_REQUESTER_ID_MASK & pTlpDrvInboundItem->dw1) >> TLP_REQUESTER_ID_LSHIFT));
				tlpQueueItem.tag = (uint8_t)((uint32_t)((TLP_TAG_MASK & pTlpDrvInboundItem->dw1) >> TLP_TAG_LSHIFT));
				tlpQueueItem.lastBE = (uint8_t)((uint32_t)((TLP_LAST_BE_MASK & pTlpDrvInboundItem->dw1) >> TLP_LAST_BE_LSHIFT));
				tlpQueueItem.firstBE = (uint8_t)((uint32_t)((TLP_FIRST_BE_MASK & pTlpDrvInboundItem->dw1) >> TLP_FIRST_BE_LSHIFT));

				tlpQueueItem.addr = ((uint32_t)((TLP_ADDRESS_MASK & pTlpDrvInboundItem->dw2) >> TLP_ADDRESS_LSHIFT));
				tlpQueueItem.data = ((uint32_t)((TLP_DATA_MASK & pTlpDrvInboundItem->dw3) >> TLP_DATA_LSHIFT));
			}
			else if(tlpQueueItem.fmt == 0x00 && tlpQueueItem.type == 0x00)
			{	// Memory write request TLP
				LOG_INFO("#### Memory read request TLP\r\n");
				tlpQueueItem.tc = (uint8_t)((uint32_t)((TLP_TC_MASK & pTlpDrvInboundItem->dw0) >> TLP_TC_LSHIFT));
				tlpQueueItem.td = (uint8_t)((uint32_t)((TLP_TD_MASK & pTlpDrvInboundItem->dw0) >> TLP_TD_LSHIFT));
				tlpQueueItem.ep = (uint8_t)((uint32_t)((TLP_EP_MASK & pTlpDrvInboundItem->dw0) >> TLP_EP_LSHIFT));
				tlpQueueItem.attr = (uint8_t)((uint32_t)((TLP_ATTR_MASK & pTlpDrvInboundItem->dw0) >> TLP_ATTR_LSHIFT));
				tlpQueueItem.length = (uint16_t)((uint32_t)((TLP_LENGTH_MASK & pTlpDrvInboundItem->dw0) >> TLP_LENGTH_LSHIFT));

				tlpQueueItem.requesterId = (uint16_t)((uint32_t)((TLP_REQUESTER_ID_MASK & pTlpDrvInboundItem->dw1) >> TLP_REQUESTER_ID_LSHIFT));
				tlpQueueItem.tag = (uint8_t)((uint32_t)((TLP_TAG_MASK & pTlpDrvInboundItem->dw1) >> TLP_TAG_LSHIFT));
				tlpQueueItem.lastBE = (uint8_t)((uint32_t)((TLP_LAST_BE_MASK & pTlpDrvInboundItem->dw1) >> TLP_LAST_BE_LSHIFT));
				tlpQueueItem.firstBE = (uint8_t)((uint32_t)((TLP_FIRST_BE_MASK & pTlpDrvInboundItem->dw1) >> TLP_FIRST_BE_LSHIFT));

				tlpQueueItem.addr = ((uint32_t)((TLP_ADDRESS_MASK & pTlpDrvInboundItem->dw2) >> TLP_ADDRESS_LSHIFT));
			}
			else if(tlpQueueItem.fmt == 0x02 && tlpQueueItem.type == 0x0A)
			{	// Memory write request TLP
				LOG_INFO("#### Memory completion request TLP\r\n");
				tlpQueueItem.tc = (uint8_t)((uint32_t)((TLP_TC_MASK & pTlpDrvInboundItem->dw0) >> TLP_TC_LSHIFT));
				tlpQueueItem.td = (uint8_t)((uint32_t)((TLP_TD_MASK & pTlpDrvInboundItem->dw0) >> TLP_TD_LSHIFT));
				tlpQueueItem.ep = (uint8_t)((uint32_t)((TLP_EP_MASK & pTlpDrvInboundItem->dw0) >> TLP_EP_LSHIFT));
				tlpQueueItem.attr = (uint8_t)((uint32_t)((TLP_ATTR_MASK & pTlpDrvInboundItem->dw0) >> TLP_ATTR_LSHIFT));
				tlpQueueItem.length = (uint16_t)((uint32_t)((TLP_LENGTH_MASK & pTlpDrvInboundItem->dw0) >> TLP_LENGTH_LSHIFT));

				tlpQueueItem.completerId = (uint16_t)((uint32_t)((TLP_COMPLETER_ID_MASK & pTlpDrvInboundItem->dw1) >> TLP_COMPLETER_ID_LSHIFT));
				tlpQueueItem.status = (uint8_t)((uint32_t)((TLP_STATUS_MASK & pTlpDrvInboundItem->dw1) >> TLP_STATUS_LSHIFT));
				tlpQueueItem.bcm = (uint8_t)((uint32_t)((TLP_BCM_MASK & pTlpDrvInboundItem->dw1) >> TLP_BCM_LSHIFT));
				tlpQueueItem.byteCount = (uint16_t)((uint32_t)((TLP_BYTE_COUNT_MASK & pTlpDrvInboundItem->dw1) >> TLP_BYTE_COUNT_LSHIFT));

				tlpQueueItem.requesterId = (uint16_t)((uint32_t)((TLP_REQUESTER_ID_MASK & pTlpDrvInboundItem->dw2) >> TLP_REQUESTER_ID_LSHIFT));
				tlpQueueItem.tag = (uint8_t)((uint32_t)((TLP_TAG_MASK & pTlpDrvInboundItem->dw2) >> TLP_TAG_LSHIFT));
				tlpQueueItem.lowerAddr = (uint8_t)((uint32_t)((TLP_LOWER_ADDRESS_MASK & pTlpDrvInboundItem->dw2) >> TLP_LOWER_ADDRESS_LSHIFT));

				tlpQueueItem.data = ((uint32_t)((TLP_DATA_MASK & pTlpDrvInboundItem->dw3) >> TLP_DATA_LSHIFT));
			}
			else
			{
				LOG_INFO("#### UNKNOWN TLP message\r\n");
			}


			LOG_INFO("#### fmt = 0x%02x\r\n", tlpQueueItem.fmt);
			LOG_INFO("#### type = 0x%02x\r\n", tlpQueueItem.type);
			LOG_INFO("#### tc = 0x%02x\r\n", tlpQueueItem.tc);
			LOG_INFO("#### td = 0x%02x\r\n", tlpQueueItem.td);
			LOG_INFO("#### ep = 0x%02x\r\n", tlpQueueItem.ep);
			LOG_INFO("#### attr = 0x%02x\r\n", tlpQueueItem.attr);
			LOG_INFO("#### length = 0x%02x\r\n", tlpQueueItem.length);

			LOG_INFO("#### requesterId = 0x%02x\r\n", tlpQueueItem.requesterId);
			LOG_INFO("#### completerId = 0x%02x\r\n", tlpQueueItem.completerId);
			LOG_INFO("#### status = 0x%02x\r\n", tlpQueueItem.status);
			LOG_INFO("#### bcm = 0x%02x\r\n", tlpQueueItem.bcm);
			LOG_INFO("#### byteCount = 0x%02x\r\n", tlpQueueItem.byteCount);
			LOG_INFO("#### tag = 0x%02x\r\n", tlpQueueItem.tag);
			LOG_INFO("#### lastBE = 0x%02x\r\n", tlpQueueItem.lastBE);
			LOG_INFO("#### firstBE = 0x%02x\r\n", tlpQueueItem.firstBE);
			LOG_INFO("#### lowerAddr = 0x%02x\r\n", tlpQueueItem.lowerAddr);

			LOG_INFO("#### addr = 0x%02x\r\n", tlpQueueItem.addr);
			LOG_INFO("#### data = 0x%02x\r\n", tlpQueueItem.data);
		}
    }
}
#endif


#endif
