/**
****************************************************************************
* @file tlpDriver.h
* @author rex
* @version 0.1
* @date 28 February 2018
* @brief TLP Driver Implementation
* @section COPYRIGHT (C) Oclaro 2008 - 2018
*
*
****************************************************************************
*/

/* Define to prevent recursive inclusion ************************************/
#ifndef SRC_DRIVERS_TLP_DRIVER_TLPDRIVER_H_
#define SRC_DRIVERS_TLP_DRIVER_TLPDRIVER_H_

/* Includes *****************************************************************/
/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "stdbool.h"

/* Function Prototypes *****************************************************/
QueueHandle_t xTlpInboundQueue;

/* Defines ******************************************************************/
// TLP packet masks
#define TLP_FMT_MASK			0x60000000		// DW 0 [30:29]
#define TLP_TYPE_MASK			0x1F000000		// DW 0 [24:28]
#define TLP_TC_MASK				0x00700000		// DW 0 [19:16]
#define TLP_TD_MASK				0x00008000		// DW 0 [15]
#define TLP_EP_MASK				0x00004000		// DW 0 [14]
#define TLP_ATTR_MASK			0x00003000		// DW 0 [14]
#define TLP_LENGTH_MASK			0x000003FF		// DW 0 [9:0]

#define TLP_REQUESTER_ID_MASK	0xFFFF0000		// DW 1 [31:16]
#define TLP_TAG_MASK			0x0000FF00		// DW 1 or DW 2 [15:8]
#define TLP_LAST_BE_MASK		0x000000F0		// DW 1 [7:4]
#define TLP_FIRST_BE_MASK		0x0000000F		// DW 1 [3:0]

#define TLP_ADDRESS_MASK		0xFFFFFFFC		// DW 2 [31:2]
#define TLP_DATA_MASK			0xFFFFFFFF		// DW 3 [31:0]

#define TLP_COMPLETER_ID_MASK	0xFFFF0000		// DW 1 [31:16]	- used by completion message
#define TLP_STATUS_MASK			0x0000E000		// DW 1 [15:13]
#define TLP_BCM_MASK			0x00001000		// DW 1 [12]
#define TLP_BYTE_COUNT_MASK 	0x00000FFF		// DW 1 [11:0]
#define TLP_LOWER_ADDRESS_MASK	0x0000007F		// DW 2 [6:0]

#define TLP_FMT_LSHIFT			29UL
#define TLP_TYPE_LSHIFT			24UL
#define TLP_TC_LSHIFT			20UL
#define TLP_TD_LSHIFT			15UL
#define TLP_EP_LSHIFT			14UL
#define TLP_ATTR_LSHIFT			12UL
#define TLP_LENGTH_LSHIFT		0UL

#define TLP_REQUESTER_ID_LSHIFT	16UL
#define TLP_TAG_LSHIFT			8UL
#define TLP_LAST_BE_LSHIFT		4UL
#define TLP_FIRST_BE_LSHIFT		0UL

#define TLP_ADDRESS_LSHIFT		2UL
#define TLP_DATA_LSHIFT			0UL

#define TLP_COMPLETER_ID_LSHIFT	16UL
#define TLP_STATUS_LSHIFT		13UL
#define TLP_BCM_LSHIFT			12UL
#define TLP_BYTE_COUNT_LSHIFT 	0UL
#define TLP_LOWER_ADDRESS_LSHIFT	0UL

/* Structures go here *******************************************************/
struct ATlpDrvQueueItem
{
	uint8_t fmt;		/// The "Fmt" field tells how long is the header, and if a data payload is present.
	uint8_t type;		/// Describes the TLP operation
	uint8_t tc;
	bool td;
	bool ep;
	uint8_t attr;
	uint16_t length;		/// The field "Length" tells how many DWs are in the payload (from 0 to 1023)
	uint16_t requesterId;
	uint16_t completerId;
	uint8_t status;
	bool bcm;
	uint16_t byteCount;
	uint8_t tag;
	uint8_t lastBE;
	uint8_t firstBE;
	uint32_t addr;
	uint8_t lowerAddr;
	uint32_t data;
}tlpDrvQueueItem;

/* Variables ****************************************************************/
/// Task handler for the incoming TLP messages task
TaskHandle_t tlpDrv_IncomingMessagesTaskHdl;

/* Public Functions Prototypes***********************************************/
bool tlpDrv_Create();

/* Public Function test Prototypes ******************************************/
void tlpDrv_test_ParseIncomingTlpMessage(uint32_t blk1,
								uint32_t blk2,
								uint32_t blk3,
								uint32_t blk4,
								struct ATlpDrvQueueItem *tlpQueueItem);

#endif /* SRC_DRIVERS_TLP_DRIVER_TLPDRIVER_H_ */

/******************************* END OF FILE ********************************/

#if 0

/*
 * tlpDriver.h
 *
 *  Created on: 22 Feb 2018
 *      Author: Rex.Taylor
 */

#ifndef SRC_DRIVERS_TLP_DRIVER_TLPDRIVER_H_
#define SRC_DRIVERS_TLP_DRIVER_TLPDRIVER_H_

bool tlpDrv_EngineInit();
void tlpDrvIncomingMessagesTask(void * arg);
void tlpDrvOutgoingMessagesTask(void * arg);
void tlpDrvTestTask(void * arg);		// for test purposes only

struct ATlpDrvQueueItem
{
	uint8_t fmt;		// The "Fmt" field tells how long is the header, and if a data payload is present.
	uint8_t type;		// Describes the TLP operation
	uint8_t tc;
	bool td;
	bool ep;
	uint8_t attr;
	uint16_t length;		// The field "Length" tells how many DWs are in the payload (from 0 to 1023)
	uint16_t requesterId;
	uint16_t completerId;
	uint8_t status;
	bool bcm;
	uint16_t byteCount;
	uint8_t tag;
	uint8_t lastBE;
	uint8_t firstBE;
	uint32_t addr;
	uint8_t lowerAddr;
	uint32_t data;
}tlpDrvQueueItem;

struct tlpWritePacket
{
	uint32_t dw0;
	uint32_t dw1;
	uint32_t dw2;
	uint32_t dw3;
};

struct tlpReadPacket
{
	uint32_t dw0;
	uint32_t dw1;
	uint32_t dw2;
};

struct tlpCompletionPacket
{
	uint32_t dw0;
	uint32_t dw1;
	uint32_t dw2;
	uint32_t dw3;
};

struct tlpInboundPacket
{
	uint32_t dw0;
	uint32_t dw1;
	uint32_t dw2;
	uint32_t dw3;
};

#endif /* SRC_DRIVERS_TLP_DRIVER_TLPDRIVER_H_ */

#endif
