/*
 * MdioMstr.h
 *
 *  Created on: 26 Oct 2017
 *      Author: Pai-Lab
 */

#ifndef SRC_MDIOMSTR_H_
#define SRC_MDIOMSTR_H_
/* Mdio master ports available */
#define MDIOMST0 0
#define MDIOMST1 1
#define MAX_WR_SIZE 256
/* structure to hold write mdio arguments
 * the Mdio port address
 * The slave register address
 * The data to write
 */
typedef  struct
{
	uint16_t mdioport;
	uint16_t strtwaddr;
	uint16_t * wdata;
	uint16_t length;
} MDIOWRPKT;
typedef struct
{
	uint16_t mdioPrt;
	uint16_t strtRaddr;
	uint16_t * rdata;
	uint16_t rlen;
}MDIOPKTR;
void InitMdio( void );
uint16_t MdioRead(uint16_t MPort,uint16_t rd_add);
void MdioWrite(MDIOWRPKT MwPrt );
void MdioBlkRead(MDIOPKTR mdioR);
#endif /* SRC_MDIOMSTR_H_ */
