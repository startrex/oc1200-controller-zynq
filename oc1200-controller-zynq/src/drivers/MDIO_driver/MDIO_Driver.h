/**
****************************************************************************
* @file MDIO_Driver.c
* @author rex
* @version 0.1
* @date 28 Feb 2018
* @brief MDIO Driver Implementation
* @section COPYRIGHT (C) Oclaro 2008 - 2018
****************************************************************************
*/

/* Define to prevent recursive inclusion ************************************/
#ifndef SRC_MDI_CONTEXT_HANDLER_H_
#define SRC_MDI_CONTEXT_HANDLER_H_

/* Includes *****************************************************************/
#include "stdbool.h"
#include "../../utils/cxtTbl.h"

/* Defines ******************************************************************/

/* Structures go here *******************************************************/

/* Public Variables ********************************************************/
QueueHandle_t xMdioInboundQueue;

/* Public Functions Prototypes **********************************************/
bool mdioDrv_Create();

#endif /* SRC_MDI_CONTEXT_HANDLER_H_ */

/******************************* END OF FILE ********************************/


#if 0

/*
 * MDIO_ContextHandler.h
 *
 *  Created on: 19th Feb, 2018
 *      Author: RexTaylor
 */

#ifndef SRC_MDI_CONTEXT_HANDLER_H_
#define SRC_MDI_CONTEXT_HANDLER_H_

#include "stdbool.h"

#include "../../cxtTbl.h"

struct AMdioQueueItem
{
	cxtTbl_t *cxtTbl;
	uint8_t cxtIdx;
}mdioQueueItem;

bool mdio_GetRegisterValue(cxt_t 		*callingCxt);
bool mdio_SetRegisterValue(cxt_t 		*callingCxt);
void mdioTask(void * arg);
bool mdio_CreateMdioQueueItem(cxtTbl_t *cxtTbl, uint8_t cxtIdx);
bool mdio_EngineInit();

#endif		// SRC_MDI_CONTEXT_HANDLER_H_

#endif
