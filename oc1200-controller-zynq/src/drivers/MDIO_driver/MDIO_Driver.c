/**
****************************************************************************
* @file MDIO_Driver.c
* @author rex
* @version 0.1
* @date 28 Feb 2018
* @brief MDIO Driver Implementation
* @section COPYRIGHT (C) Oclaro 2008 - 2018
****************************************************************************
*/
/** @addtogroup Drivers
* @{
*/
/** @addtogroup MDIO_Driver
* @{
*/
/* Includes *****************************************************************/
/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "stdbool.h"
#include "../../utils/log.h"

#include "MDIO_Driver.h"
#include "../TLP_Driver/tlpDriver.h"
#include "MdioMstr.h"

/* Defines ******************************************************************/
#define MDIO_READ_CMD			(0x00)		/// Command specified by TLP packet is READ
#define MDIO_WRITE_CMD			(0x01)		/// Command specified by TLP packet is WRITE
/* Private Variables ********************************************************/
TaskHandle_t mdioTaskHdl = NULL;


/* Private Function Prototypes **********************************************/
void mdioTask(void * arg);


/**
 * @brief The MDIO driver task
 *
 * @return none
 */
void mdioTask(void * arg)
{
	uint16_t cmdType = 0x00;
	uint16_t value = 0x00;
	MDIOWRPKT MdioWriteReadPckt;

	struct ATlpDrvQueueItem *pTlpInboundQueueItem;

	InitMdio();

	// init the mdio queue
	xMdioInboundQueue = xQueueCreate( 10, sizeof( struct ATlpDrvQueueItem * ) );
	if(xMdioInboundQueue == NULL)
	{	// ERROR case
		LOG_INFO("ERROR - Failed to create xMdioInboundQueue\r\n");
		return;
	}

	while(true)
	{
		if(xQueueReceive( xMdioInboundQueue, &(pTlpInboundQueueItem), (TickType_t) portMAX_DELAY))
		{
			if(pTlpInboundQueueItem->fmt == 0x02 && pTlpInboundQueueItem->type == 0x00)
			{	// write command
				cmdType = MDIO_WRITE_CMD;
				MdioWriteReadPckt.length = 0x01;
				MdioWriteReadPckt.mdioport = 0x01;
				MdioWriteReadPckt.strtwaddr = pTlpInboundQueueItem->addr;
				*MdioWriteReadPckt.wdata = pTlpInboundQueueItem->data;

				//value = MdioRead(0x01, (uint16_t) pTlpInboundQueueItem->addr);
				MdioWrite(MdioWriteReadPckt);
				//value = MdioRead(0x01, (uint16_t) pTlpInboundQueueItem->addr);
			}
			else if(pTlpInboundQueueItem->fmt == 0x00 && pTlpInboundQueueItem->type == 0x00)
			{	// read command
				cmdType = MDIO_READ_CMD;
				/*MdioWriteReadPckt.length = 0x01;
				MdioWriteReadPckt.mdioport = 0x00;
				MdioWriteReadPckt.strtwaddr = pTlpInboundQueueItem->addr;*/

				value = MdioRead(0x01, (uint16_t) pTlpInboundQueueItem->addr);
				pTlpInboundQueueItem->data = value;
			}
			else
			{

			}

			if(pTlpInboundQueueItem->fmt == 0x00 && pTlpInboundQueueItem->type == 0x00)
			{	// only need to reply when read is requested - respond via TLP Completion message
				if(xQueueSendToBack( xTlpInboundQueue, ( void * ) &pTlpInboundQueueItem, ( TickType_t ) 0 ) == 0x00)
				{
					LOG_INFO(">>> ERROR in pushing to  xTlpDrvQueue\r\n");
				}
			}
		}
	}
}
#if 0
bool mdio_GetRegisterValue(cxt_t 		*callingCxt)
{
	uint16_t port = callingCxt->callingArgs[0];
	uint16_t addr = callingCxt->callingArgs[1];
	uint16_t value = 0x00;
//#if 0
	value = MdioRead(port, addr);
//#endif
	callingCxt->returnArgCount = 0x01;
	callingCxt->returnArgs[0x00] = value;

	LOG_INFO(">>> MDIO Read 0x%02x\r\n", value);

	return true;
}

bool mdio_SetRegisterValue(cxt_t 		*callingCxt)
{
	MDIOWRPKT MwPrt;

	MwPrt.mdioport = callingCxt->callingArgs[0];
	MwPrt.strtwaddr = callingCxt->callingArgs[1];
	MwPrt.wdata = &(callingCxt->callingArgs[2]);
	MwPrt.length = 0x01;

//#if 0
	MdioWrite(MwPrt);
//#endif

	LOG_INFO(">>> MDIO Write 0x%02x\r\n", MwPrt.wdata);

	return true;
}
#endif

/**
 * @brief Creation of the MDIO Driver
 * @return bool - true = successful
 */
bool mdioDrv_Create()
{
	BaseType_t retval = pdFAIL;

	retval = xTaskCreate( mdioTask,
		( const char * ) "mdio",
		configMINIMAL_STACK_SIZE,
		NULL,
		tskIDLE_PRIORITY + 3,
		&mdioTaskHdl );

	if(retval == pdPASS)
	{
		return true;
	}

	LOG_INFO("FAILED to create - mdioTask\r\n");
	return false;
}


//@} end <module_name>/
//@} end <module_group>/
/******************************* END OF FILE ********************************/

#if 0

/*
 * 	MDIO_ContextHandler.c
 * 	Handle context command interface to MDIO layer
 *	Oclaro Technology 2018
 *
 *
 */

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "MDIO_Driver.h"
#include "MdioMstr.h"

#include "../../log.h"
#include "../../deque.h"
#include "../../cxtTbl.h"
#include "../../hCmdConst.h"
#include "../../cCmdLib.h"
#include "../../errorCode.h"
#include "../../hlf.h"
#include "../../ahLib.h"

TaskHandle_t mdioTaskHdl = NULL;

#define DELAY_10_SECONDS	10000UL
#define DELAY_1_SECOND		1000UL


static QueueHandle_t xMdioQueue = NULL;

void mdioTask(void * arg)
{
	//MDIO_QUEUE_ITEM *mdioQueueItem = NULL;
	uint8_t cxtIdx = 0x00;
	uint16_t cmdType = 0x00;
	uint8_t opticsChannel = 0x00;
	uint16_t addr = 0x00;
	uint16_t data = 0x00;

	cxt_t 		*callingCxt;
	int16_t 	status;
	UBaseType_t queueCount;
	ahPkt_t     *ahPkt;

	const TickType_t x1seconds = pdMS_TO_TICKS( DELAY_1_SECOND );

	uint16_t ulNotifiedValue = 0x00;
	struct AMdioQueueItem *pMessageQueueItem = NULL;

	LOG_INFO("M D I O  T A S K  R U N N I N G\r\n");

	// init the mdio queue
	xMdioQueue = xQueueCreate( 10, sizeof( struct MDIO_QUEUE_ITEM * ) );
	if(xMdioQueue == NULL)
	{	// ERROR case
		LOG_INFO("ERROR - Failed to create xMdioQueue\r\n");
		return;
	}

	while( xQueueReceive( acoCallingQueue, ahPkt, ( TickType_t ) portMAX_DELAY ) == pdTRUE)
	{
		queueCount = uxQueueMessagesWaiting( acoCallingQueue );
		LOG_INFO("In acoTestTask: Post Recieve acoCallingQueue: %d!\r\n", queueCount);

		callingCxt = &(cStore.hlfTbl->tbl[ahPkt->callingCxtIdx]);

		LOG_INFO("In acoTestTask: ArgCount: %d\r\n", callingCxt->callingArgCount);
		for(int idx=0; idx < callingCxt->callingArgCount; idx++) {
			LOG_INFO("In acoTestTask: Args[%d]: %d\r\n", idx, callingCxt->callingArgs[idx]);
		}


		cmdType = callingCxt->cmdType;
		LOG_INFO(">>> cmdType = %d\r\n", cmdType);

		switch(cmdType)
		{
		case HCMD_ACOGETREG:
			mdio_GetRegisterValue(callingCxt);
			break;

		case HCMD_ACOSETREG:
			mdio_SetRegisterValue(callingCxt);
			break;
		}

		//vTaskDelay( x1seconds );

		//Set return values from ACO
		LOG_INFO("In acoTestTask: setting cxt to ready\r\n");
		callingCxt->returnArgCount = 1;
		callingCxt->callingArgs[0] = SUCCESS;
		//Set ready flag in cxt and increment semaphore
		cxtTblSetReadyCxtTrue(cStore.hlfTbl, ahPkt->callingCxtIdx);
		LOG_INFO("In acoTestTask: Completed setting cxt to ready\r\n");

	}

#if 0
	//Endless loop
	for( ;; )
    {
		if(xQueueReceive( xMdioQueue, &(pMessageQueueItem), (TickType_t) portMAX_DELAY))
		{
			LOG_INFO(">>> mdio Queue item received\r\n");
			LOG_INFO(">>> mdio Queue free spaces after pop %d\r\n", uxQueueSpacesAvailable(xMdioQueue));

			cxtIdx = pMessageQueueItem->cxtIdx;
			cmdType = pMessageQueueItem->cxtTbl->tbl[cxtIdx].cmdType;

			LOG_INFO(">>> cmdType = %d\r\n", cmdType);

			LOG_INFO(">>> callingArgCount = %d\r\n", pMessageQueueItem->cxtTbl->tbl[cxtIdx].callingArgCount);
			for(uint8_t i = 0x00; i < pMessageQueueItem->cxtTbl->tbl[cxtIdx].callingArgCount; i++)
			{
				LOG_INFO(">>> callingArgs = %d %02x\r\n", i, pMessageQueueItem->cxtTbl->tbl[cxtIdx].callingArgs[i]);
			}

			switch(cmdType)
			{
			case HCMD_ACOGETREG:
				mdio_GetRegisterValue(pMessageQueueItem->cxtTbl, pMessageQueueItem->cxtIdx);
				break;

			case HCMD_ACOSETREG:
				mdio_SetRegisterValue(pMessageQueueItem->cxtTbl, pMessageQueueItem->cxtIdx);
				break;
			}

			cxtTblSetReadyCxtTrue(pMessageQueueItem->cxtTbl, pMessageQueueItem->cxtIdx);
		}
    }
#endif
}

bool mdio_CreateMdioQueueItem(cxtTbl_t *cxtTbl, uint8_t cxtIdx)
{
	bool retval = true;
	struct AMdioQueueItem *pMessageQueueItem = NULL;
	struct AMdioQueueItem mdioQueueItem;

	mdioQueueItem.cxtTbl = cxtTbl;
	mdioQueueItem.cxtIdx = cxtIdx;

	LOG_INFO(">>> mdioQueue free spaces before push %d\r\n", uxQueueSpacesAvailable(xMdioQueue));

	pMessageQueueItem = &mdioQueueItem;
	if(xQueueSendToBack( xMdioQueue, ( void * ) &pMessageQueueItem, ( TickType_t ) 0 ) == 0x00)
	{
		LOG_INFO(">>> ERROR in pushing to  mdioQueue\r\n");
		retval = false;
	}

	return retval;
}

bool mdio_GetRegisterValue(cxt_t 		*callingCxt)
{
	uint16_t port = callingCxt->callingArgs[0];
	uint16_t addr = callingCxt->callingArgs[1];
	uint16_t value = 0x00;
//#if 0
	value = MdioRead(port, addr);
//#endif
	callingCxt->returnArgCount = 0x01;
	callingCxt->returnArgs[0x00] = value;

	LOG_INFO(">>> MDIO Read 0x%02x\r\n", value);

	return true;
}

bool mdio_SetRegisterValue(cxt_t 		*callingCxt)
{
	MDIOWRPKT MwPrt;

	MwPrt.mdioport = callingCxt->callingArgs[0];
	MwPrt.strtwaddr = callingCxt->callingArgs[1];
	MwPrt.wdata = &(callingCxt->callingArgs[2]);
	MwPrt.length = 0x01;

//#if 0
	MdioWrite(MwPrt);
//#endif

	LOG_INFO(">>> MDIO Write 0x%02x\r\n", MwPrt.wdata);

	return true;
}

bool mdio_EngineInit()
{
	xTaskCreate( mdioTask,
			 ( const char * ) "mdio",
			 configMINIMAL_STACK_SIZE,
			 NULL,
			 tskIDLE_PRIORITY + 3,
			 &mdioTaskHdl );

	return true;
}



#endif
