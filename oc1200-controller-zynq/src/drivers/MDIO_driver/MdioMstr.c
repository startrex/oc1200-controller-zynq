/*
 * 	MdioMstr.c
 * 	Mdio Master to ACO slave code for Zynq S.o.C.
 *	Oclaro Technology 2017
 *  
 *     
 */
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xil_io.h"
#include "xstatus.h"
#include "MdioMstr.h"

#define FABRIC_BASE_ADDRESS                   0x43C00000
#define STD_BASE_ADDRESS                      FABRIC_BASE_ADDRESS
#define HIF_BASE_ADDRESS                      FABRIC_BASE_ADDRESS + 0x1000
#define SER_BASE_ADDRESS                      FABRIC_BASE_ADDRESS + 0x2000
#define PCI_BASE_ADDRESS                      FABRIC_BASE_ADDRESS + 0x3000

#define STD_SCRATCH_ADDR0                     STD_BASE_ADDRESS
#define STD_SCRATCH_ADDR1                     STD_BASE_ADDRESS + 0x4
#define STD_DEBUG_ADDR                        STD_BASE_ADDRESS + 0x8
#define STD_FPGA_REV_ADDR                     STD_BASE_ADDRESS + 0x10
#define STD_SW_RESET_INTERNAL_ADDR            STD_BASE_ADDRESS + 0x20
#define STD_SW_RESET_EXTERNAL_ADDR            STD_BASE_ADDRESS + 0x24
#define STD_IRQ_STAT_ADDR                     STD_BASE_ADDRESS + 0x30

#define SER_SCRATCH_ADDR0                     SER_BASE_ADDRESS
#define SER_SCRATCH_ADDR1                     SER_BASE_ADDRESS + 0x4
#define SER_SERIAL_IRQ_ADDR                   SER_BASE_ADDRESS + 0x14
#define SER_MDIO_MAST_0_CTRL_DATA_ADDR        SER_BASE_ADDRESS + 0x100
#define SER_MDIO_MAST_0_WRITE_DATA_ADDR       SER_BASE_ADDRESS + 0x104
#define SER_MDIO_MAST_0_READ_DATA_ADDR        SER_BASE_ADDRESS + 0x108
#define SER_MDIO_MAST_0_PREAMBLE_ADDR         SER_BASE_ADDRESS + 0x10C
#define SER_MDIO_MAST_0_STATUS_ADDR           SER_BASE_ADDRESS + 0x110
#define SER_MDIO_MAST_0_CONFIG_ADDR           SER_BASE_ADDRESS + 0x114
#define SER_MDIO_MAST_1_CTRL_DATA_ADDR        SER_BASE_ADDRESS + 0x120
#define SER_MDIO_MAST_1_WRITE_DATA_ADDR       SER_BASE_ADDRESS + 0x124
#define SER_MDIO_MAST_1_READ_DATA_ADDR        SER_BASE_ADDRESS + 0x128
#define SER_MDIO_MAST_1_PREAMBLE_ADDR         SER_BASE_ADDRESS + 0x12C
#define SER_MDIO_MAST_1_STATUS_ADDR           SER_BASE_ADDRESS + 0x130
#define SER_MDIO_MAST_1_CONFIG_ADDR           SER_BASE_ADDRESS + 0x134
#define SER_MDIO_SLAVE_0_CTRL_DATA_ADDR       SER_BASE_ADDRESS + 0x140
#define SER_MDIO_SLAVE_0_DATA_TO_MASTER_ADDR  SER_BASE_ADDRESS + 0x144
#define SER_MDIO_SLAVE_0_DATA_FRM_MASTER_ADDR SER_BASE_ADDRESS + 0x148
#define SER_MDIO_SLAVE_0_ADDR_FRM_MASTER_ADDR SER_BASE_ADDRESS + 0x14C

#define MDIOPRT0	SER_MDIO_MAST_0_WRITE_DATA_ADDR
#define MDIOPRT1	SER_MDIO_MAST_1_WRITE_DATA_ADDR
#define MDIOSLV		SER_MDIO_SLAVE_0_ADDR_FRM_MASTER_ADDR
#define DEFAULT_CFP2_TB_ADD 5
#define AOPCODE 0		/* opcode for a address */
#define WOPCODE	1		/* opcode for a write */
#define RPINCCODE 2		/* opcode for read with post increment */
#define ROPCODE 3		/* opcode for a read */

#define TAR 0			/* turn around bits for read */
#define TAW 2			/* turn around bits for write */

typedef struct
{
	uint32_t ctrl_data_add;
	uint32_t wr_data_add;
	uint32_t sts_add;
	uint32_t mdiordata;
}MDIOADDRS;
/* private fn defns */
int check_busy(uint32_t status_address);
uint32_t MdioPack(uint32_t opcode,uint32_t turnaround);
MDIOADDRS MdioPrtAdds(uint32_t MdiPrt);
/* file scope vars */


	uint32_t regad = 1;
	uint32_t phyad = DEFAULT_CFP2_TB_ADD;
	uint32_t st = 1;
	uint32_t  ctrl_val;
	uint32_t failcount=0;
	uint32_t passcount=0;
	MDIOWRPKT MdioWPkt;
	MDIOPKTR mdioRpkt;

	/**
	  * @brief  InitMdio Initialise the MDIO
	  * @param none
	  * @return uint32_t return success
	  */
void  InitMdio( void )
{
	Xil_Out32(SER_MDIO_SLAVE_0_CTRL_DATA_ADDR, 0x000);
	Xil_Out32(SER_MDIO_SLAVE_0_DATA_TO_MASTER_ADDR, 0x8765);

	// The following are some defaults for the control flags
	regad = 1;
	st = 0;

}
/**
  * @brief  MdioRead Read a 16 bit register from the ACO
  * @param uint16_t MPort Master Mdio to read, uint16_t read address  *
  * @return uint16_t The data in the ACO register
  */
uint16_t MdioRead(uint16_t MPort,uint16_t rd_add)
{
	MDIOADDRS mdioaddR;

	/* configure mdio settings for selected port */
	mdioaddR = MdioPrtAdds(MPort);

	check_busy(mdioaddR.sts_add);

	// Start with writing the address
	ctrl_val = MdioPack(AOPCODE,TAR);
	Xil_Out32(mdioaddR.wr_data_add, rd_add);
	Xil_Out32(mdioaddR.ctrl_data_add, ctrl_val);

	check_busy(mdioaddR.sts_add);

	// Now read from the port
	ctrl_val = MdioPack(ROPCODE,TAR);
	Xil_Out32(mdioaddR.ctrl_data_add, ctrl_val);

	check_busy(mdioaddR.sts_add);
	return (uint16_t)Xil_In32(mdioaddR.mdiordata);

}
void MdioBlkRead(MDIOPKTR mdioR)
{
	MDIOADDRS mdioaddR;

	/* configure mdio settings for selected port */
	mdioaddR = MdioPrtAdds(mdioR.mdioPrt);
	check_busy(mdioaddR.sts_add);

	// Start with writing the address
	ctrl_val = MdioPack(AOPCODE,TAR);
	Xil_Out32(mdioaddR.wr_data_add, mdioR.strtRaddr);
	Xil_Out32(mdioaddR.ctrl_data_add, ctrl_val);

	check_busy(mdioaddR.sts_add);

	while(mdioR.rlen--)
	{
		// Now read from the port
		ctrl_val = MdioPack(RPINCCODE,TAR);
		Xil_Out32(mdioaddR.ctrl_data_add, ctrl_val);

		check_busy(mdioaddR.sts_add);
		*mdioR.rdata++ = (uint16_t)Xil_In32(mdioaddR.mdiordata);
	}



}
/**
  * @brief  MdioWrite write  a 16 bit register to the ACO
  * @param 16bit struct containing Mdio Master port, address to write to and pointer to data to write
  * @return none
  */
void MdioWrite(MDIOWRPKT MwPrt )
{
	MDIOADDRS mdioaddW;
	uint16_t mdadd = MwPrt.strtwaddr;

	/* configure mdio settings for selected port */
	mdioaddW = MdioPrtAdds(MwPrt.mdioport);

	while(MwPrt.length--)
	{
		check_busy(mdioaddW.sts_add);

		// Start with writing the address
		ctrl_val = MdioPack(AOPCODE,TAR);
		Xil_Out32(mdioaddW.wr_data_add, mdadd++);
		Xil_Out32(mdioaddW.ctrl_data_add, ctrl_val);

		check_busy(mdioaddW.sts_add);

		// Now send the new data
		ctrl_val = MdioPack(WOPCODE,TAW);
		Xil_Out32(mdioaddW.wr_data_add, *MwPrt.wdata++);
		Xil_Out32(mdioaddW.ctrl_data_add, ctrl_val);
	}
}

uint32_t MdioPack(uint32_t opcode,uint32_t turnaround)
{
	return turnaround | (regad << 2) | (phyad << 7) | (opcode << 12) | (st << 14) ;
}
MDIOADDRS MdioPrtAdds(uint32_t MdiPrt)
{
	MDIOADDRS mdioadds;
	switch(MdiPrt)
		{
		case MDIOMST0:
			mdioadds.ctrl_data_add   = SER_MDIO_MAST_0_CTRL_DATA_ADDR;
			mdioadds.wr_data_add = SER_MDIO_MAST_0_WRITE_DATA_ADDR;
			mdioadds.sts_add = SER_MDIO_MAST_0_STATUS_ADDR;
			mdioadds.mdiordata = SER_MDIO_MAST_0_READ_DATA_ADDR;
			break;

		case MDIOMST1:
			mdioadds.ctrl_data_add   = SER_MDIO_MAST_1_CTRL_DATA_ADDR;
			mdioadds.wr_data_add = SER_MDIO_MAST_1_WRITE_DATA_ADDR;
			mdioadds.sts_add = SER_MDIO_MAST_1_STATUS_ADDR;
			mdioadds.mdiordata = SER_MDIO_MAST_1_READ_DATA_ADDR;
			break;

		default:
			break;
		}
	return mdioadds;
}

int check_busy(uint32_t status_address)
{
	// Someday this polling routine should be replaced with an interrupt?
	uint32_t timeout = 1000;
	while((Xil_In32(status_address) > 0) && (timeout > 0))
	{
		timeout -= 1;
		/*if(timeout == 0)
			xil_printf("  ERROR - MDIO_BUSY never went low!\n\r");*/
	}
	return 0;
}





