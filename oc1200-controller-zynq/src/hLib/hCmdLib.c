/*
 * hlfLib.c
 *
 *  Created on: Jan 15, 2018
 *      Author: pgrant
 */

#include "FreeRTOS.h"

#include "hCmdLib.h"
#include "../ahLib/ahLib.h"
#include "hCmdConst.h"
#include "../utils/errorCode.h"
#include "../utils/log.h"
#include "../utils/cxtTbl.h"


//HLF Command State
#define STATE_HACOSETREG 	0x2000
#define STATE_HACOGETREG 	0x2001
#define STATE_READYCPSMCXT 	0x1002

int16_t hLibACOSetReg_nextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx) {

	switch (cxtTbl->tbl[cxtIdx].cmdState)  {
		case STATE_NONE:
			LOG_INFO("In hLibACOSetReg_nextState: STATE_NONE\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_HACOSETREG;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		case STATE_HACOSETREG:
			LOG_INFO("In hLibACOSetReg_nextState: STATE_HACOSETREG\r\n");
			//Do set of aco reg
			ahMDIOWrite(cxtIdx, cxtTbl->tbl[cxtIdx].callingArgs[0], cxtTbl->tbl[cxtIdx].callingArgs[1]);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_READYCPSMCXT;
			return SUCCESS;
			break;
		case STATE_READYCPSMCXT:
			LOG_INFO("In hLibACOSetReg_nextState: STATE_READYCPSMCXT\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_RELEASECXT;
			//Set ready on cpsm calling cxt
			cxtTblSetReadyCxtTrue(cStore.cpsmTbl, cxtTbl->tbl[cxtIdx].callingCPSMCxt);
			//Set ready on hlf cxt
			//cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		case STATE_RELEASECXT:
			LOG_INFO("In hLibACOSetReg_nextState: STATE_RELEASECXT\r\n");
			cxtTblFreeCxt(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		default:
			LOG_INFO("In hLibACOSetReg_nextState: Error: not a defined state!!\r\n");
			return ERROR;
			break;
	}
}

int16_t hLibACOGetReg_nextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx) {

	switch (cxtTbl->tbl[cxtIdx].cmdState)  {
		case STATE_NONE:
			LOG_INFO("In hLibACOGetReg_nextState: STATE_NONE\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_HACOGETREG;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		case STATE_HACOGETREG:
			LOG_INFO("In hLibACOGetReg_nextState: STATE_HACOGETREG\r\n");
			//Do get of aco reg
			ahMDIORead(cxtIdx, cxtTbl->tbl[cxtIdx].callingArgs[0]);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_READYCPSMCXT;
			return SUCCESS;
			break;
		case STATE_READYCPSMCXT:
			LOG_INFO("In hLibACOSetReg_nextState: STATE_READYCPSMCXT\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_RELEASECXT;
			//Set ready on cpsm calling cxt
			cxtTblSetReadyCxtTrue(cStore.cpsmTbl, cxtTbl->tbl[cxtIdx].callingCPSMCxt);
			//Set ready on hlf cxt
			//cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		case STATE_RELEASECXT:
			LOG_INFO("In hLibACOGetReg_nextState: STATE_RELEASECXT\r\n");
			cxtTblFreeCxt(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		default:
			LOG_INFO("In hLibACOGetReg_nextState: Error: not a defined state!!\r\n");
			return ERROR;
			break;
	}
}

int16_t hLibCallCmdNextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx, uint16_t cmdType) {
	switch (cmdType)  {
	case HCMD_ACOSETREG:
		hLibACOSetReg_nextState(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case HCMD_ACOPOLLREG:
		hLibACOPollReg_nextState(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case HCMD_ACOGETREG:
		hLibACOSetReg_nextState(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case HCMD_NONE:
		LOG_ERROR("In hLibCallCmdNextState: Error: cmd NONE should not be defined!!\r\n");
		return ERROR;
		break;
	default:
		LOG_ERROR("In hLibCallCmdNextState: Error: No known cmd defined!!\r\n");
		return ERROR;
		break;
	}
}

int16_t hLibInitCxt(cxtTbl_t *cxtTbl, uint8_t cxtIdx, uint16_t cmdType, pciPkt_t *pciPkt) {
	return hLibCallCmdNextState(cxtTbl, cxtIdx, cmdType);
}

int16_t hLibDoNextAction(cxtTbl_t *cxtTbl, uint8_t cxtIdx) {
	uint16_t cmdType = cxtTbl->tbl[cxtIdx].cmdType;
	return hLibCallCmdNextState(cxtTbl, cxtIdx, cmdType);
}

int16_t hLibCallHLFCmd(uint16_t hCmdType, int32_t *hCmdArgs, uint8_t hCmdArgCount, int16_t callingCPSMCxt) {
	uint16_t nextFreeCxtIdx;
	int16_t status;

	LOG_INFO("In hLibCallHLFCmd\r\n");
	nextFreeCxtIdx = cxtTblGetNextFreeCxtIdx(cStore.hlfTbl);
	if (nextFreeCxtIdx != ERROR) {
		cStore.hlfTbl->tbl[nextFreeCxtIdx].cmdType = hCmdType;
		cStore.hlfTbl->tbl[nextFreeCxtIdx].cmdState = STATE_NONE;
		cStore.hlfTbl->tbl[nextFreeCxtIdx].callingArgCount = hCmdArgCount;
		for(int idx=0; idx < hCmdArgCount; idx++) {
			cStore.hlfTbl->tbl[nextFreeCxtIdx].callingArgs[idx] = hCmdArgs[idx];
		}
		cStore.hlfTbl->tbl[nextFreeCxtIdx].returnArgCount = 0;
		cStore.hlfTbl->tbl[nextFreeCxtIdx].callingCPSMCxt = callingCPSMCxt;

		cxtTblSetReadyCxtTrue(cStore.hlfTbl, nextFreeCxtIdx);
		return nextFreeCxtIdx;
	} else {
    	LOG_ERROR("In hLibCallHLFCmd: Failed to get free hlf command context!\r\n");
    	return ERROR;
	}

}

int16_t hLibCheckHLFCmdStatus(uint8_t cxtIdx) {
	LOG_INFO("In hLibCheckHLFCmdStatus\r\n");
	if ((cStore.hlfTbl->tbl[cxtIdx].returnArgCount >= 0) && (cStore.hlfTbl->tbl[cxtIdx].returnArgs[0] == SUCCESS)) {
		return SUCCESS;
	} else {
		return ERROR;
	}
}

int16_t hLibReleaseHLFCmdCxt(uint8_t cxtIdx) {
	LOG_INFO("In hLibReleaseHLFCmdCxt: %d\r\n", cxtIdx);
	cxtTblSetReadyCxtTrue(cStore.hlfTbl, cxtIdx);
	return cxtIdx;

}


