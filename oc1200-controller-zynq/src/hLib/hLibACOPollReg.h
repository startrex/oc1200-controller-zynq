/*
 * hLibACOPollSuccess.h
 *
 *  Created on: Feb 25, 2018
 *      Author: root
 */

#ifndef SRC_HLIBACOPOLLREG_H_
#define SRC_HLIBACOPOLLREG_H_

int16_t hLibACOPollReg_nextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx);


#endif /* SRC_HLIBACOPOLLREG_H_ */
