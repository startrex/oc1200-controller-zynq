/*
 * hCmdConst.h
 *
 *  Created on: 30 Jan 2018
 *      Author: Paul.Grant
 */

#ifndef SRC_HCMDCONST_H_
#define SRC_HCMDCONST_H_

//HLF Command Types
#define HCMD_NONE 			0x0000
#define HCMD_ACOSETREG 		0x0001
#define HCMD_ACOGETREG 		0x0002
#define HCMD_ACOPOLLREG 	0x0003

//HLF Command State
#define STATE_HACOSETREG 	0x2000
#define STATE_HACOGETREG 	0x2001


#endif /* SRC_HCMDCONST_H_ */
