/*
 * hlfLib.h
 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */

#ifndef SRC_HCMDLIB_H_
#define SRC_HCMDLIB_H_

#include "FreeRTOS.h"

#include "../utils/cxtTbl.h"
#include "../utils/deque.h"

int16_t hLibInitCxt(cxtTbl_t *cxtTbl, uint8_t cxtIdx, uint16_t cmdType, pciPkt_t *pciPkt);
int16_t hLibDoNextAction(cxtTbl_t *cxtTbl, uint8_t cxtIdx);
int16_t hLibCallHLFCmd(uint16_t hCmdType, int32_t *hCmdArgs, uint8_t hCmdArgCount, int16_t callingCPSMCxt);
int16_t hLibCheckHLFCmdStatus(uint8_t cxtIdx);
int16_t hLibReleaseHLFCmdCxt(uint8_t cxtIdx);


#endif /* SRC_HCMDLIB_H_ */
