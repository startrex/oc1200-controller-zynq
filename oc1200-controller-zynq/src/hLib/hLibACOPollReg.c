/*
 * hLibACOPollReg.c
 *
 *  Created on: Feb 25, 2018
 *      Author: root
 */

#include "../ahLib/acoRegConst.h"
#include "hCmdConst.h"
#include "../utils/cxtTbl.h"
#include "../utils/errorCode.h"
#include "../utils/log.h"


#define INITIALVALUE 	0x00000000
#define DELAY_1_SECOND		1000UL

//Cmd states
#define STATE_HACOREADSTATUS 		0x1000
#define STATE_HCHECKSTATUS 			0x1001
#define STATE_READYCPSMCXT 			0x1002
#define STATE_RELEASECXT 			0x1003

int16_t hLibACOPollReg_nextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx) {
	int32_t pollingRegAddr;
	int32_t statusRegValue;

	const TickType_t x1seconds = pdMS_TO_TICKS( DELAY_1_SECOND );

	switch (cxtTbl->tbl[cxtIdx].cmdState)  {
		case STATE_NONE:
			LOG_INFO("In hLibACOPollReg_nextState: STATE_NONE\r\n");

			cxtTbl->tbl[cxtIdx].cmdState = STATE_HACOREADSTATUS;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		case STATE_HACOREADSTATUS:
			LOG_INFO("In hLibACOPollReg_nextState: STATE_HACOREADSTATUS\r\n");
			//Do Read Status Reg
			pollingRegAddr  = cxtTbl->tbl[cxtIdx].callingArgs[1];
			ahMDIORead(cxtIdx, cxtTbl->tbl[cxtIdx].callingArgs[0]);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_HCHECKSTATUS;
			return SUCCESS;
			break;
		case STATE_HCHECKSTATUS:
			LOG_INFO("In hLibACOPollReg_nextState: STATE_HCHECKSTATUS\r\n");
			//Check returned status status
			statusRegValue  = cxtTbl->tbl[cxtIdx].returnArgs[1];
			if (statusRegValue == INITIALVALUE) {
				//Waiting for polling interval
		    	//vTaskDelay( x1seconds );
				cxtTbl->tbl[cxtIdx].cmdState = STATE_HACOREADSTATUS;
				cxtTbl->tbl[cxtIdx].returnArgCount = 0;
				cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
				return SUCCESS;
			} else {
				cxtTbl->tbl[cxtIdx].cmdState = STATE_READYCPSMCXT;
				cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
				return SUCCESS;
			}
			break;
		case STATE_READYCPSMCXT:
			LOG_INFO("In hLibACOPollReg_nextState: STATE_READYCPSMCXT\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_RELEASECXT;
			//Set ready on cpsm calling cxt
			cxtTblSetReadyCxtTrue(cStore.cpsmTbl, cxtTbl->tbl[cxtIdx].callingCPSMCxt);
			//Set ready on hlf cxt
			//cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		case STATE_RELEASECXT:
			LOG_INFO("In hLibACOPollReg_nextState: STATE_RELEASECXT\r\n");
			cxtTblFreeCxt(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		default:
			LOG_INFO("In hLibACOPollReg_nextState: Error: not a defined state!!\r\n");
			return ERROR;
			break;
	}
}


