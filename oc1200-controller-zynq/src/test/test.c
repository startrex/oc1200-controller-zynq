/*
 * test.c
 *
 *  Created on: Jan 17, 2018
 *      Author: pgrant
 */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "../ahLib/acoRegConst.h"
#include "../ahLib/ahLib.h"
#include "../cLib/cCmdConst.h"
#include "../utils/cxtTbl.h"
#include "../utils/deque.h"
#include "../utils/errorCode.h"
#include "../utils/log.h"

#define DELAY_10_SECONDS	10000UL
#define DELAY_1_SECOND		1000UL

pciPkt_t testHostInPkts[] = {
		{ CCMD_ACOASYNCCF, 0, { } },
		{ CCMD_ACOSETREG, 2, { ACO_FINEFREQ_ADDR, 3} },
		{ CCMD_ACOSETREG, 2, { ACO_COURSEFREQ_ADDR, 2} }
//		{ CCMD_ACOASYNCCF, 0, { } },
//		{ CCMD_ACOSETREG, 3, { ACO_COURSEFREQ_ADDR, 6, 6} },
//		{ CCMD_ACOSETREG, 3, { ACO_FINEFREQ_ADDR, 7, 7} },
//		{ CCMD_ACOASYNCCF, 0, { } },
//		{ CCMD_ACOASYNCCF, 0, {} },
//		{ CCMD_ACOSETREG, 3, { ACO_COURSEFREQ_ADDR, 10, 10} },
//		{ CCMD_ACOSETREG, 3, { ACO_FINEFREQ_ADDR, 11, 11} },
//		{ CCMD_ACOASYNCCF, 0, { } },
//		{ CCMD_ACOASYNCCF, 0, { } }
};

uint16_t testHostInPktCount = 3;

void ppHostTestTask(void * arg)
{

	const TickType_t x10seconds = pdMS_TO_TICKS( DELAY_10_SECONDS );

	uint16_t currentIndexTestPkt = 0;
	queueCont_t *currentCont;
	pciPkt_t *currentPCIPkt;
	uint16_t currentIndex;
	UBaseType_t semCount;

	//Populate hostIn queue with test pkts
    while(currentIndexTestPkt < testHostInPktCount) {
    	//Get next queue container
    	currentIndex = dequeGetCurrentIndex(qStore.hostInQueue);
    	currentCont = (queueCont_t *) qStore.hostInQueue->queue[currentIndex];

    	//Initialise pkt with next host test packet
    	currentPCIPkt = &currentCont->pciPkt;
    	currentPCIPkt->cmdType = testHostInPkts[currentIndexTestPkt].cmdType;
    	currentPCIPkt->cmdArgCount = testHostInPkts[currentIndexTestPkt].cmdArgCount;
    	for(int idx=0; idx < currentPCIPkt->cmdArgCount; idx++) {
    		currentPCIPkt->cmdArgs[idx] = testHostInPkts[currentIndexTestPkt].cmdArgs[idx];
    	}

    	//Push container onto queue
    	dequePush(qStore.hostInQueue);
    	xSemaphoreGive( qStore.hostInQueue->newCmdSem );
    	semCount = uxSemaphoreGetCount( qStore.hostInQueue->newCmdSem );
    	LOG_INFO("In testTask: Post give newCmdSem: %d!\r\n", semCount);

    	currentIndexTestPkt = currentIndexTestPkt + 1;
    }



    //Endless loop
    while (1) {
    	vTaskDelay( x10seconds );
    }
}

void acoTestTask(void * arg)
{

	cxt_t 		*callingCxt;
	int16_t 	status;
	UBaseType_t queueCount;
	ahPkt_t     *ahPkt;
	int			asyncCFSuccessCount = 0;

	const TickType_t x1seconds = pdMS_TO_TICKS( DELAY_1_SECOND );

	LOG_INFO("In acoTestTask: started!\r\n");

    while( xQueueReceive( acoCallingQueue, ahPkt, ( TickType_t ) portMAX_DELAY ) == pdTRUE)
    {
    	queueCount = uxQueueMessagesWaiting( acoCallingQueue );
    	LOG_INFO("In acoTestTask: Post Recieve acoCallingQueue: %d!\r\n", queueCount);

    	callingCxt = &(cStore.hlfTbl->tbl[ahPkt->callingCxtIdx]);

    	LOG_INFO("In acoTestTask: Cmd: %d, ArgCount: %d\r\n", ahPkt->cmdType, ahPkt->argCount);
    	for(int idx=0; idx < callingCxt->callingArgCount; idx++) {
        	LOG_INFO("In acoTestTask: Args[%d]: %d\r\n", idx, callingCxt->callingArgs[idx]);
    	}

    	switch (ahPkt->cmdType) {
			case AH_MDIO_READ:
				switch (ahPkt->args[0]) {
					case ACO_ASYNCCFSUCCESS_ADDR:
						if (asyncCFSuccessCount > 2) {
							callingCxt->returnArgCount = 2;
							callingCxt->returnArgs[0] = SUCCESS;
							callingCxt->returnArgs[1] = SUCCESS;
						} else {
							callingCxt->returnArgCount = 2;
							callingCxt->returnArgs[0] = SUCCESS;
							callingCxt->returnArgs[1] = INITIALVALUE;
						}
						asyncCFSuccessCount = asyncCFSuccessCount + 1;
						break;
					default:
						LOG_WARNING("WARNING: In acoTestTask: Read of unexpected address!\r\n");
						callingCxt->returnArgCount = 1;
						callingCxt->returnArgs[0] = ERROR;
						break;
				}
				break;
			case AH_MDIO_WRITE:
				switch (ahPkt->args[0]) {
					case ACO_ASYNCCF_ADDR:
					case ACO_ASYNCCFREG1_ADDR:
					case ACO_ASYNCCFREG2_ADDR:
					case ACO_ASYNCCFREG3_ADDR:
					case ACO_DISABLELASER_ADDR:
					case ACO_ENABLELASER_ADDR:
					case ACO_COURSEFREQ_ADDR:
					case ACO_FINEFREQ_ADDR:
						callingCxt->returnArgCount = 1;
						callingCxt->returnArgs[0] = SUCCESS;
						break;
					default:
						LOG_WARNING("WARNING: In acoTestTask: Read of unexpected address!\r\n");
						callingCxt->returnArgCount = 1;
						callingCxt->returnArgs[0] = ERROR;
						break;
				}
				break;
			default:
				LOG_ERROR("ERROR: In acoTestTask: Unknown command type\r\n");
				break;
    	}
    	//vTaskDelay( x1seconds );

    	//Set ready flag in cxt and increment semaphore
       	LOG_INFO("In acoTestTask: setting cxt to ready\r\n");
		cxtTblSetReadyCxtTrue(cStore.hlfTbl, ahPkt->callingCxtIdx);
    	LOG_INFO("In acoTestTask: Completed setting cxt to ready\r\n");

    }
}





