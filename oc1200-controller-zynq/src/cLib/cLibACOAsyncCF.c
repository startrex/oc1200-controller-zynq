/*
 * cLibACOAsyncCF.c
 *
 *  Created on: Feb 24, 2018
 *      Author: root
 */

#include "../../src/cLib/cLibACOAsyncCF.h"

#include "FreeRTOS.h"

#include "../ahLib/acoRegConst.h"
#include "../cLib/cCmdConst.h"
#include "../hLib/hCmdConst.h"
#include "../hLib/hCmdLib.h"
#include "../utils/errorCode.h"
#include "../utils/log.h"
#include "../utils/cxtTbl.h"

//Cmd states
#define STATE_CCALCFREQ 			0x1000
#define STATE_CSETREG1 				0x1001
#define STATE_CSETREG1_STATUS 		0x1002
#define STATE_CSETREG2 				0x1003
#define STATE_CSETREG2_STATUS 		0x1004
#define STATE_CSETREG3 				0x1005
#define STATE_CSETREG3_STATUS 		0x1006
#define STATE_CDISABLELASER 		0x1007
#define STATE_CDISABLELASER_STATUS 	0x1008
#define STATE_CSETCHANGEFREQ 		0x1009
#define STATE_CSETCHANGEFREQ_STATUS 0x100A
#define STATE_POLLCFSUCCESS 		0x100B
#define STATE_POLLCFSUCCESS_STATUS 	0x100C
#define STATE_CENABLELASER 			0x100D
#define STATE_CENABLELASER_STATUS 	0x100E


int16_t cLibACOAsyncCF_nextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx) {
	int16_t hlfCmdCxtIdx;

	switch (cxtTbl->tbl[cxtIdx].cmdState)  {
	case STATE_NONE:
		cxtTbl->tbl[cxtIdx].cmdType = CCMD_ACOASYNCCF;
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_NONE!\r\n");
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CCALCFREQ;
		cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case STATE_CCALCFREQ:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CCALCFREQ!\r\n");
		int32_t freqArg  = cxtTbl->tbl[cxtIdx].callingArgs[1];
		int32_t calcFreq = freqArg * 10;
		//value for reg1 write
		cxtTbl->tbl[cxtIdx].state[1] = calcFreq + 10;
		//value for reg1 write
		cxtTbl->tbl[cxtIdx].state[2] = calcFreq + 20;
		//value for reg1 write
		cxtTbl->tbl[cxtIdx].state[3] = calcFreq + 30;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CDISABLELASER;
		cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case STATE_CDISABLELASER:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CDISABLELASER!\r\n");
		cxtTbl->tbl[cxtIdx].returnArgs[0] = ACO_DISABLELASER_ADDR;
		cxtTbl->tbl[cxtIdx].returnArgs[1] = 0x00000000;
		hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOSETREG, cxtTbl->tbl[cxtIdx].returnArgs, 2, cxtIdx);
		//Save hlf cxt
		cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CDISABLELASER_STATUS;
		return SUCCESS;
		break;
	case STATE_CDISABLELASER_STATUS:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CDISABLELASER_STATUS!\r\n");
		//Check success of HCMD_ACOSETREG
		hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
		if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_CSETREG1;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
		} else {
			LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState:  HLF error in STATE_CDISABLELASER_STATUS\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return ERROR;
		}
	case STATE_CSETREG1:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CSETREG1!\r\n");
		cxtTbl->tbl[cxtIdx].returnArgs[0] = ACO_ASYNCCFREG1_ADDR;
		cxtTbl->tbl[cxtIdx].returnArgs[1] = cxtTbl->tbl[cxtIdx].state[1];
		hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOSETREG, cxtTbl->tbl[cxtIdx].returnArgs, 2, cxtIdx);
		//Save hlf cxt
		cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CSETREG1_STATUS;
		return SUCCESS;
		break;
	case STATE_CSETREG1_STATUS:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CSETREG1_STATUS!\r\n");
		hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
		if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_CSETREG2;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
		} else {
			LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: HLF error in STATE_CSETREG1\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return ERROR;
		}
	case STATE_CSETREG2:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CSETREG2!\r\n");
		cxtTbl->tbl[cxtIdx].returnArgs[0] = ACO_ASYNCCFREG2_ADDR;
		cxtTbl->tbl[cxtIdx].returnArgs[1] = cxtTbl->tbl[cxtIdx].state[2];
		hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOSETREG, cxtTbl->tbl[cxtIdx].returnArgs, 2, cxtIdx);
		//Save hlf cxt
		cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CSETREG2_STATUS;
		return SUCCESS;
		break;
	case STATE_CSETREG2_STATUS:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CSETREG2_STATUS!\r\n");
		hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
		if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_CSETREG3;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
		} else {
			LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: HLF error in STATE_CSETREG2\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return ERROR;
		}
	case STATE_CSETREG3:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CSETREG3!\r\n");
		cxtTbl->tbl[cxtIdx].returnArgs[0] = ACO_ASYNCCFREG3_ADDR;
		cxtTbl->tbl[cxtIdx].returnArgs[1] = cxtTbl->tbl[cxtIdx].state[3];
		hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOSETREG, cxtTbl->tbl[cxtIdx].returnArgs, 2, cxtIdx);
		//Save hlf cxt
		cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CSETREG3_STATUS;
		return SUCCESS;
		break;
	case STATE_CSETREG3_STATUS:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CSETREG3_STATUS!\r\n");
		hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
		if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_CSETCHANGEFREQ;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
		} else {
			LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: HLF error in STATE_CSETREG3\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return ERROR;
		}
	case STATE_CSETCHANGEFREQ:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CSETCHANGEFREQ!\r\n");
		cxtTbl->tbl[cxtIdx].returnArgs[0] = ACO_ASYNCCF_ADDR;
		cxtTbl->tbl[cxtIdx].returnArgs[1] = 0x11111111;
		hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOSETREG, cxtTbl->tbl[cxtIdx].returnArgs, 2, cxtIdx);
		//Save hlf cxt
		cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CSETCHANGEFREQ_STATUS;
		return SUCCESS;
		break;
	case STATE_CSETCHANGEFREQ_STATUS:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CSETCHANGEFREQ_STATUS!\r\n");
		hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
		if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_POLLCFSUCCESS;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
		} else {
			LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: HLF error in STATE_CSETCHANGEFREQ\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return ERROR;
		}
	case STATE_POLLCFSUCCESS:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_POLLCFSUCCESS!\r\n");
		cxtTbl->tbl[cxtIdx].returnArgs[0] = ACO_ASYNCCFSUCCESS_ADDR;
		hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOPOLLREG, cxtTbl->tbl[cxtIdx].returnArgs, 2, cxtIdx);
		//Save hlf cxt
		cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_POLLCFSUCCESS_STATUS;
		return SUCCESS;
		break;
	case STATE_POLLCFSUCCESS_STATUS:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_POLLCFSUCCESS_STATUS!\r\n");
		hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
		if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
			//Check value returned by MDIO poll
			if (cStore.hlfTbl->tbl[hlfCmdCxtIdx].returnArgs[1] == SUCCESS) {
				LOG_INFO("In cLibACOAsyncCF_nextState: AsyncCF Successfully!!!\r\n");
			} else {
				LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: AsyncCF Failed!!!\r\n");
				//Todo handle error
			}
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_CENABLELASER;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
		} else {
			LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: HLF error in STATE_CSETCHANGEFREQ\r\n");
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return ERROR;
		}
	case STATE_CENABLELASER:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CENABLELASER!\r\n");
		cxtTbl->tbl[cxtIdx].returnArgs[0] = ACO_ENABLELASER_ADDR;
		cxtTbl->tbl[cxtIdx].returnArgs[2] = 0x00000000;
		hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOSETREG, cxtTbl->tbl[cxtIdx].returnArgs, 2, cxtIdx);
		//Save hlf cxt
		cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CENABLELASER_STATUS;
		return SUCCESS;
		break;
	case STATE_CENABLELASER_STATUS:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_CENABLELASER_STATUS!\r\n");
		hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
		if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
		} else {
			LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: HLF error in STATE_CENABLELASER\r\n");
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return ERROR;
		}
	case STATE_TIDYUP:
		LOG_INFO("In cLibACOAsyncCF_nextState: STATE_TIDYUP\r\n");
		cxtTbl->tbl[cxtIdx].cmdState = STATE_RELEASECXT;
		cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case STATE_RELEASECXT:
		LOG_INFO("In cLibACOAsyncCF_nextState: "
				"STATE_RELEASECXT\r\n");
		cxtTblFreeCxt(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	default:
		LOG_INFO("In cLibACOAsyncCF_nextState: Error: not a defined state!!\r\n");
		return ERROR;
		break;
	}
}
