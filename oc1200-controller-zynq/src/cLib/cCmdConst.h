/*
 * cCmdConst.h
 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */

#ifndef SRC_CCMDCONST_H_
#define SRC_CCMDCONST_H_

//CPSM Command Types
#define CCMD_NONE 				0x0000
#define CCMD_ACOGETREG 			0x0001
#define CCMD_ACOSETREG 			0x0002
#define CCMD_ACOASYNCCF 		0x0003

//CPSM Command State
#define STATE_CACOSET 			0x1000
#define STATE_CACOSET_STATUS 	0x1001
#define STATE_CACOGET 			0x1002
#define STATE_CACOGET_STATUS 	0x1003
#define STATE_CACOASYNCCF 		0x1004

#endif /* SRC_CCMDCONST_H_ */

