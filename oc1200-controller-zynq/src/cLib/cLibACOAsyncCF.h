/*
 * cLibACOAsyncCF.h
 *
 *  Created on: Feb 24, 2018
 *      Author: root
 */

#ifndef SRC_CLIBACOASYNCCF_H_
#define SRC_CLIBACOASYNCCF_H_

#include "FreeRTOS.h"

#include "../utils/cxtTbl.h"

int16_t cLibACOAsyncCF_nextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx);

#endif /* SRC_CLIBACOASYNCCF_H_ */
