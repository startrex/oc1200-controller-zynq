/*
 * cpsmCmdLib.c
 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */
#include "../../src/cLib/cCmdLib.h"

#include "FreeRTOS.h"

#include "../cLib/cCmdConst.h"
#include "../cLib/cLibACOAsyncCF.h"
#include "../hLib/hCmdConst.h"
#include "../hLib/hCmdLib.h"
#include "../utils/errorCode.h"
#include "../utils/log.h"
#include "../utils/cxtTbl.h"

//Include cLib command

/*
 * Change frequency command
 */

int16_t cLibACOGetReg_nextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx) {
	int16_t hlfCmdCxtIdx;

	switch (cxtTbl->tbl[cxtIdx].cmdState)  {
		case STATE_NONE:
			cxtTbl->tbl[cxtIdx].cmdType = CCMD_ACOGETREG;
			LOG_INFO("In cLibACOGetReg_nextState: STATE_NONE\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_CACOSET;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		case STATE_CACOGET:
			LOG_INFO("In cLibACOGetReg_nextState: STATE_CACOGET\r\n");
			hlfCmdCxtIdx = hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOGETREG, cxtTbl->tbl[cxtIdx].callingArgs, cxtTbl->tbl[cxtIdx].callingArgCount, cxtIdx);
			//Save hlf cxt
			cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
			cxtTbl->tbl[cxtIdx].cmdState = STATE_CACOGET_STATUS;
			return SUCCESS;
			break;
		case STATE_CACOGET_STATUS:
			LOG_INFO("In cLibACOGetReg_nextState: STATE_CACOGET_STATUS\r\n");
			hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
			if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
				hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
				cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
				cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
				return SUCCESS;
			} else {
				LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: HLF error in STATE_CACOGET\r\n");
				hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
				cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
				cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
				return ERROR;
			}
		case STATE_TIDYUP:
			LOG_INFO("In cLibACOGetReg_nextState: STATE_TIDYUP\r\n");
			cxtTbl->tbl[cxtIdx].cmdState = STATE_RELEASECXT;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		case STATE_RELEASECXT:
			LOG_INFO("In cLibACOGetReg_nextState: Doing STATE_RELEASECXT\r\n");
			cxtTblFreeCxt(cxtTbl, cxtIdx);
			return SUCCESS;
			break;
		default:
			LOG_INFO("In cLibACOGetReg_nextState: Error: not a defined state!!\r\n");
			return ERROR;
			break;
	}
}

int16_t cLibACOSetReg_nextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx) {
	int16_t hlfCmdCxtIdx;

	switch (cxtTbl->tbl[cxtIdx].cmdState)  {
	case STATE_NONE:
		cxtTbl->tbl[cxtIdx].cmdType = CCMD_ACOSETREG;
		LOG_INFO("In cLibACOSetReg: STATE_NONE\r\n");
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CACOSET;
		cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case STATE_CACOSET:
		LOG_INFO("In cLibACOSetReg: STATE_CACOSET\r\n");
		hlfCmdCxtIdx = hLibCallHLFCmd(HCMD_ACOSETREG, cxtTbl->tbl[cxtIdx].callingArgs, cxtTbl->tbl[cxtIdx].callingArgCount, cxtIdx);
		//Save hlf cxt
		cxtTbl->tbl[cxtIdx].state[0] = hlfCmdCxtIdx;
		cxtTbl->tbl[cxtIdx].cmdState = STATE_CACOSET_STATUS;
		return SUCCESS;
		break;
	case STATE_CACOSET_STATUS:
		LOG_INFO("In cLibACOGetReg_nextState: STATE_CACOSET_STATUS\r\n");
		hlfCmdCxtIdx = cxtTbl->tbl[cxtIdx].state[0];
		if (hLibCheckHLFCmdStatus(hlfCmdCxtIdx) == SUCCESS) {
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return SUCCESS;
		} else {
			LOG_ERROR("ERROR: In cLibACOAsyncCF_nextState: HLF error in STATE_CACOSET\r\n");
			hLibReleaseHLFCmdCxt(hlfCmdCxtIdx);
			cxtTbl->tbl[cxtIdx].cmdState = STATE_TIDYUP;
			cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
			return ERROR;
		}
	case STATE_TIDYUP:
		LOG_INFO("In cLibACOSetReg: STATE_TIDYUP\r\n");
		cxtTbl->tbl[cxtIdx].cmdState = STATE_RELEASECXT;
		cxtTblSetReadyCxtTrue(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case STATE_RELEASECXT:
		LOG_INFO("In cLibACOSetReg: Doing STATE_RELEASECXT\r\n");
		cxtTblFreeCxt(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	default:
		LOG_INFO("In cLibACOSetReg: Error: not a defined state!!\r\n");
		return ERROR;
		break;
	}
}



int16_t cLibCallCmdNextState(cxtTbl_t *cxtTbl, uint8_t cxtIdx, uint16_t cmdType) {
	switch (cmdType)  {
	case CCMD_ACOGETREG:
		cLibACOGetReg_nextState(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case CCMD_ACOSETREG:
		cLibACOSetReg_nextState(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case CCMD_ACOASYNCCF:
		cLibACOAsyncCF_nextState(cxtTbl, cxtIdx);
		return SUCCESS;
		break;
	case CCMD_NONE:
		LOG_INFO("In cLibCallCmdNextState: Error: cmd NONE should not be defined!!\r\n");
		return ERROR;
		break;
	default:
		LOG_INFO("In cLibCallCmdNextState: Error: No known cmd defined!!\r\n");
		return ERROR;
		break;
	}
}

int16_t cpsmCLibInitCxt(cxtTbl_t *cxtTbl, uint8_t cxtIdx, uint16_t cmdType) {
	return cLibCallCmdNextState(cxtTbl, cxtIdx, cmdType);
}

int16_t cpsmCLibDoNextAction(cxtTbl_t *cxtTbl, uint8_t cxtIdx) {
	uint16_t cmdType = cxtTbl->tbl[cxtIdx].cmdType;
	return cLibCallCmdNextState(cxtTbl, cxtIdx, cmdType);
}


