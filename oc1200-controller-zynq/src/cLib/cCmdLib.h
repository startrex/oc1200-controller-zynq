/*
 * cpsmCmdLib.h
 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */

#ifndef SRC_CCMDLIB_H_
#define SRC_CCMDLIB_H_

#include "FreeRTOS.h"

#include "../../src/utils/cxtTbl.h"
#include "../../src/utils/deque.h"


int16_t cpsmCLibInitCxt(cxtTbl_t *cxtTbl, uint8_t cxtIdx, uint16_t cmdType);
int16_t cpsmCLibDoNextAction(cxtTbl_t *cxtTbl, uint8_t cxtIdx);


#endif /* SRC_CCMDLIB_H_ */
