/*
    FreeRTOS V8.2.1 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    1 tab == 4 spaces!

    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?".  Have you defined configASSERT()?  *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *   Investing in training allows your team to be as productive as       *
     *   possible as early as possible, lowering your overall development    *
     *   cost, and enabling you to bring a more robust product to market     *
     *   earlier than would otherwise be possible.  Richard Barry is both    *
     *   the architect and key author of FreeRTOS, and so also the world's   *
     *   leading authority on what is the world's most popular real time     *
     *   kernel for deeply embedded MCU designs.  Obtaining your training    *
     *   from Richard ensures your team will gain directly from his in-depth *
     *   product knowledge and years of usage experience.  Contact Real Time *
     *   Engineers Ltd to enquire about the FreeRTOS Masterclass, presented  *
     *   by Richard Barry:  http://www.FreeRTOS.org/contact
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *    You are receiving this top quality software for free.  Please play *
     *    fair and reciprocate by reporting any suspected issues and         *
     *    participating in the community forum:                              *
     *    http://www.FreeRTOS.org/support                                    *
     *                                                                       *
     *    Thank you!                                                         *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org - Documentation, books, training, latest versions,
    license and Real Time Engineers Ltd. contact details.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
/* Xilinx includes. */
#include "xparameters.h"

#include "utils/deque.h"
#include "utils/log.h"
#include "cengine/cpsm.h"
#include "cengine/hlf.h"
#include "ahlib/ahLib.h"
#include "test/test.h"

//#include "drivers/MDIO_driver/MdioMstr.h"
//#include "drivers/MDIO_driver/MDIO_Driver.h"
//#include "drivers/TLP_driver/tlpDriver.h"
//#include "drivers/UART_Driver/uartDriver.h"


#define TIMER_ID	1
#define DELAY_10_SECONDS	10000UL
#define DELAY_1_SECOND		1000UL
#define TIMER_CHECK_THRESHOLD	9



static TaskHandle_t cpsmNewCmdTaskHdl;
static TaskHandle_t cpsmProcCmdTaskHdl;
static TaskHandle_t hlfTaskHdl;
static TaskHandle_t ahTaskHdl;
static TaskHandle_t testTaskHdl;
uint32_t initErrCtr = 0;

int main( void )
{
	//todo: logging task!
	
	const TickType_t x10seconds = pdMS_TO_TICKS( DELAY_10_SECONDS );
	uint16_t value = 0x00;
	//MDIOWRPKT mdiWr;
	uint16_t data = 0x3344;

	LOG_INFO( "OC12FW Started!\r\n" );

	//Init qStore & cStore
	dequeInitQueue();
	cxtTblInitCStore();

	//Init ahDriver
	ahDriverInit();

#if 0
	LOG_INFO( "	Calling InitMDIO\r\n" );

	InitMdio();
	while(value != 0x3344)
	{
		value = MdioRead(0x01, 0xBC00);
		mdiWr.mdioport = 0x01;
		mdiWr.strtwaddr = 0xBC00;
		mdiWr.wdata = &data;
		mdiWr.length = 0x01;

		MdioWrite(mdiWr);
		value = MdioRead(0x01, 0xBC00);
	}

	LOG_INFO( "	MDIO good to go\r\n" );
#endif

	//Create CPSM processing tasks
	xTaskCreate(cpsmNewCmdTask, 					/* The function that implements the task. */
				( const char * ) "cpsm", 	/* Text name for the task, provided to assist debugging only. */
				configMINIMAL_STACK_SIZE, 	/* The stack allocated to the task. */
				NULL, 						/* The task parameter is not used, so set to NULL. */
				tskIDLE_PRIORITY,			/* The task runs at the idle priority. */
				&cpsmNewCmdTaskHdl );

	xTaskCreate(cpsmProcCmdTask, 					/* The function that implements the task. */
				( const char * ) "cpsm", 	/* Text name for the task, provided to assist debugging only. */
				configMINIMAL_STACK_SIZE, 	/* The stack allocated to the task. */
				NULL, 						/* The task parameter is not used, so set to NULL. */
				tskIDLE_PRIORITY,			/* The task runs at the idle priority. */
				&cpsmProcCmdTaskHdl );

	//Create HLF processing tasks
	xTaskCreate( hlfProcCmdTask,
				 ( const char * ) "hlf",
				 configMINIMAL_STACK_SIZE,
				 NULL,
				 tskIDLE_PRIORITY + 1,
				 &hlfTaskHdl );

//	xTaskCreate( ahTask,
//				 ( const char * ) "ah",
//				 configMINIMAL_STACK_SIZE,
//				 NULL,
//				 tskIDLE_PRIORITY + 2,
//				 &ahTaskHdl );

	//Create PP Host Test harness
	xTaskCreate( ppHostTestTask,
				 ( const char * ) "pph",
				 configMINIMAL_STACK_SIZE,
				 NULL,
				 tskIDLE_PRIORITY + 3,
				 &testTaskHdl );

	//Create ACO Test harness
	xTaskCreate( acoTestTask,
				 ( const char * ) "aco",
				 configMINIMAL_STACK_SIZE,
				 NULL,
				 tskIDLE_PRIORITY + 3,
				 &testTaskHdl );

	//Create ACO Test harness
	/*xTaskCreate( acoTestTask,
				 ( const char * ) "aco",
				 configMINIMAL_STACK_SIZE,
				 NULL,
				 tskIDLE_PRIORITY + 3,
				 &testTaskHdl );*/
#if 0
	// Create mdio task
	mdioDrv_Create();

	// create tlpDrv task
	tlpDrv_Create();

	initErrCtr += InitUart()         == ERR_NONE ? 0 : (1L<<1);
#endif
	/* Start the tasks and timer running. */
	vTaskStartScheduler();

	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	for( ;; );
}


