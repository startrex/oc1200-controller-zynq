/*
 * cpsm.h
 *
 *  Created on: Jan 17, 2018
 *      Author: pgrant
 */

#ifndef SRC_CPSM_H_
#define SRC_CPSM_H_

void cpsmNewCmdTask(void * arg);
void cpsmProcCmdTask(void * arg);

#endif /* SRC_CPSM_H_ */
