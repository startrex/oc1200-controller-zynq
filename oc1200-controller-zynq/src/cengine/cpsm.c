/*
 * cpsm.c

 *
 *  Created on: Jan 15, 2018
 *      Author: pgrant
 */

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "semphr.h"

#include "../../src/ahLib/acoRegConst.h"
#include "../../src/cLib/cCmdConst.h"
#include "../../src/cLib/cCmdLib.h"
#include "../../src/utils/errorCode.h"
#include "../../src/utils/log.h"
#include "../utils/deque.h"
#include "../utils/cxtTbl.h"

void cpsmNewCmdTask(void * arg)
{
	uint32_t 	currentQueueIndex;
	pciPkt_t 	*cmdPkt;
	queueCont_t *queueCont;
	uint8_t 	freeCxtIdx;
    int16_t  	status;
	UBaseType_t semCount;

	LOG_INFO("In cpsmNewCmdTask: started!\r\n");

    while( xSemaphoreTake( qStore.hostInQueue->newCmdSem, ( TickType_t ) portMAX_DELAY ) == pdTRUE)
    {
    	currentQueueIndex = dequePop(qStore.hostInQueue);
    	semCount = uxSemaphoreGetCount( qStore.hostInQueue->newCmdSem );
    	LOG_INFO("In cpsmNewCmdTask: Post take newCmdSem: %d!\r\n", semCount);

		queueCont = (queueCont_t *) qStore.hostInQueue->queue[currentQueueIndex];
		cmdPkt = &queueCont->pciPkt;
		freeCxtIdx = cxtTblGetNextFreeCxtIdx(cStore.cpsmTbl);
		if (freeCxtIdx != ERROR) {
			cStore.cpsmTbl->tbl[freeCxtIdx].callingArgCount = cmdPkt->cmdArgCount;
			for(int idx=0; idx < cmdPkt->cmdArgCount; idx++) {
				cStore.cpsmTbl->tbl[freeCxtIdx].callingArgs[idx] = cmdPkt->cmdArgs[idx];
			}
			status = cpsmCLibInitCxt(cStore.cpsmTbl, freeCxtIdx, cmdPkt->cmdType);
			if (status != SUCCESS) {
				LOG_WARNING("In cpsmNewCmdTask: cpsmCLibInitCxt return error!\r\n");
				//Todo: Need to add exception handling
			}
		} else {
			LOG_ERROR("In cpsmNewCmdTask: Failed to get free cpsm command context!\r\n");
		}
	   	LOG_INFO("In cpsmNewCmdTask: New cxt init complete! \r\n");
   }
}


void cpsmProcCmdTask(void * arg)
{
	uint8_t 	nextReadyCxtIdx;
	int16_t 	status;
	UBaseType_t semCount;

	LOG_INFO("In cpsmProcCmdTask: started!\r\n");

    while( xSemaphoreTake( cStore.cpsmTbl->cmdReadySem, ( TickType_t ) portMAX_DELAY ) == pdTRUE)
    {
    	semCount = uxSemaphoreGetCount( cStore.cpsmTbl->cmdReadySem );
    	LOG_INFO("In cpsmProcCmdTask: Post take CPSM cmdReadySem: %d!\r\n", semCount);

    	nextReadyCxtIdx = cxtTblGetNextReadyCxtIdx(cStore.cpsmTbl);
		cxtTblSetReadyCxtFalse(cStore.cpsmTbl, nextReadyCxtIdx);
    	if (nextReadyCxtIdx != ERROR) {
			status = cpsmCLibDoNextAction(cStore.cpsmTbl, nextReadyCxtIdx);
			if (status == ERROR) {
				LOG_ERROR("ERROR: In cpsmProcCmdTask: cpsmCLibDoNextAction return error!\r\n");
				//Todo: Need to add exception handling
			}
    	} else {
        	LOG_ERROR("In cpsmNewCmdTask: Failed to get free cpsm command context!\r\n");
    	}

    	LOG_INFO("In cpsmProcCmdTask: Processing ready cxt complete! \r\n");

    }
}

