/*
 * hlf.c
 *
 *  Created on: Feb 8, 2018
 *      Author: pgrant
 */


/* FreeRTOS includes. */
#include "../../src/cengine/hlf.h"

#include "FreeRTOS.h"
#include "semphr.h"

#include "../../src/cLib/cCmdConst.h"
#include "../../src/cLib/cCmdLib.h"
#include "../../src/utils/cxtTbl.h"
#include "../../src/utils/deque.h"
#include "../../src/utils/errorCode.h"
#include "../../src/utils/log.h"




void hlfProcCmdTask(void * arg)
{
	uint8_t 	nextReadyCxtIdx;
	int16_t 	status;
	UBaseType_t semCount;

	LOG_INFO("In hlfProcCmdTask: started!\r\n");

    while( xSemaphoreTake( cStore.hlfTbl->cmdReadySem, ( TickType_t ) portMAX_DELAY ) == pdTRUE)
    {
    	semCount = uxSemaphoreGetCount( cStore.hlfTbl->cmdReadySem );
    	LOG_INFO("In hlfProcCmdTask: Post take HLF cmdReadySem: %d!\r\n", semCount);

    	nextReadyCxtIdx = cxtTblGetNextReadyCxtIdx(cStore.hlfTbl);
		cxtTblSetReadyCxtFalse(cStore.hlfTbl, nextReadyCxtIdx);
    	if (nextReadyCxtIdx != ERROR) {
			status = hLibDoNextAction(cStore.hlfTbl, nextReadyCxtIdx);
			if (status == ERROR) {
				LOG_WARNING("In hlfProcCmdTask: hlfCLibDoNextAction return error!\r\n");
				//Todo: Need to add exception handling
			}
    	} else {
        	LOG_ERROR("In hlfProcCmdTask: Failed to get free HLF command context!\r\n");
    	}

    	LOG_INFO("In hlfProcCmdTask: Processing ready HLF cxt complete! \r\n");

    }
}

