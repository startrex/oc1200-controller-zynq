/*
 * hlf.h
 *
 *  Created on: Feb 8, 2018
 *      Author: pgrant
 */

#ifndef SRC_HLF_H_
#define SRC_HLF_H_

void hlfNewCmdTask(void * arg);
void hlfProcCmdTask(void * arg);

#endif /* SRC_HLF_H_ */
