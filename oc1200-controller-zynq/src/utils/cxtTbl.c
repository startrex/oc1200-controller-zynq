/*
 * cxtTable.c
 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */

#include "../../src/utils/cxtTbl.h"

#include "../../src/utils/errorCode.h"
#include "../../src/utils/log.h"

cStore_t cStore;

char hlfTag[] = "HLF:";
char cpsmTag[] = "CPSM:";

/* Instantiates a new deque_type.
 * Returns a pointer to a new deque_type with memory allocated by malloc,
 * returns NULL if it fails.
 */

/*
 * Initialise elements of cpsm cxt table, with reasonable initial values
 */

void cxtTblInitCPSMCTbl(cxtTbl_t *cxtTbl) {
    //LOG_DEBUG("in cxtTblInitCPSMCTbl\r\n");

    //Init cpsm cxt table
	for(uint8_t idx = 0; idx < MAX_TABLE_SIZE; idx++) {
		cxtTbl->tbl[idx].isCPSMCxt = true;
		cxtTbl->tbl[idx].inUse = false;
		cxtTbl->tbl[idx].ready = false;
		cxtTbl->tbl[idx].cmdType = CMD_NONE;
		cxtTbl->tbl[idx].cmdState = STATE_NONE;
		cxtTbl->tbl[idx].callingCPSMCxt = -1;
		cxtTbl->tbl[idx].callingArgCount = -1;
		cxtTbl->tbl[idx].returnArgCount = -1;
	}

	//Init ready counting semaphore for cpsm cxt table
	cxtTbl->cmdReadySem = xSemaphoreCreateCounting( MAX_TABLE_SIZE, 0 );

	if( cxtTbl->cmdReadySem == NULL )
	{
		LOG_ERROR("In cxtTblInitCPSMCTbl: Init of cmdReadySem semaphore failed!\r\n");
	}

	//Init table integrity mutex for cpsm cxt table
	cxtTbl->tblMutex = xSemaphoreCreateMutex();

	if( cxtTbl->tblMutex == NULL )
	{
		LOG_ERROR("In cxtTblInitCPSMCTbl: Init of tblMutex mutex failed!\r\n");
	}


}

/*
 *  Initialise elements of hlf cxt table, with reasonable initial values
 */

void cxtTblInitHLFCTbl(cxtTbl_t *cxtTbl) {
    LOG_DEBUG("in cxtTblIntHLFCTbl\r\n");

    //Init HLF cxt table
    for(uint8_t idx = 0; idx < MAX_TABLE_SIZE; idx++) {
		cxtTbl->tbl[idx].inUse = false;
		cxtTbl->tbl[idx].ready = false;
		cxtTbl->tbl[idx].cmdType = CMD_NONE;
		cxtTbl->tbl[idx].cmdState = STATE_NONE;
		cxtTbl->tbl[idx].callingCPSMCxt = -1;
		cxtTbl->tbl[idx].callingArgCount = -1;
		cxtTbl->tbl[idx].returnArgCount = -1;
		LOG_DEBUG("in cxtTblIntHLFCTbl: %d : %d\r\n", idx, cxtTbl->tbl[idx].inUse);
	}

	//Init ready counting semaphore for cpsm cxt table
	cxtTbl->cmdReadySem = xSemaphoreCreateCounting( MAX_TABLE_SIZE, 0 );

	if( cxtTbl->cmdReadySem == NULL )
	{
		LOG_ERROR("In cxtTblInitCPSMCTbl: Init of cmdReadySem semaphore failed!\r\n");
	}

	//Init table integrity mutex for cpsm cxt table
	cxtTbl->tblMutex = xSemaphoreCreateMutex();

	if( cxtTbl->tblMutex == NULL )
	{
		LOG_ERROR("In cxtTblInitCPSMCTbl: Init of tblMutex mutex failed!\r\n");
	}

}

void cxtTblInitCStore() {
    LOG_INFO("in cxtTblInitCStore\r\n");

    //Allocate tables
    cxtTbl_t *cpsmTbl = malloc(sizeof(cxtTbl_t));
    //LOG_DEBUG("td ptr: %p\r\n", cpsmTbl);
    cxtTbl_t *hlfTbl = malloc(sizeof(cxtTbl_t));
    //LOG_DEBUG("td ptr: %p\r\n", hlfTbl);

    //Init CPSM table
	cxtTblInitCPSMCTbl(cpsmTbl);

	//Init HLF Table
	cxtTblInitHLFCTbl(hlfTbl);


    //Init cstore
    cStore.cpsmTbl = cpsmTbl;
    cStore.hlfTbl = hlfTbl;

}
/*
 *  find next free cxt in table, otherwise return error -1
 */

int16_t cxtTblGetNextFreeCxtIdx(cxtTbl_t *cxtTbl) {
	char *tag;

	if (cxtTbl == cStore.cpsmTbl) {
		tag = cpsmTag;
	} else {
		tag = hlfTag;
	}

	if ( xSemaphoreTake(cxtTbl->tblMutex, portMAX_DELAY) == pdTRUE ) {
		//LOG_DEBUG("in %s:cxtTblGetNextFreeCxtIdx\r\n", tag);
		int16_t idx = 0;

		while (idx < MAX_TABLE_SIZE) {
			if (cxtTbl->tbl[idx].inUse == false) {
				//Free cxt found, return idx
				LOG_DEBUG("In %s:cxtTblGetNextFreeCxtIdx: %d\r\n", tag, idx);
				cxtTbl->tbl[idx].inUse = true;
				xSemaphoreGive(cxtTbl->tblMutex);
				return idx;
			}
			LOG_DEBUG("In %s:cxtTblGetNextFreeCxtIdx: next cxt:%d: %d\r\n", tag, idx, cxtTbl->tbl[idx].inUse);
			idx = idx + 1;
		}
		//No free cxt return error (-1);
		LOG_DEBUG("In %s:cxtTblGetNextFreeCxtIdx: Unable to get free cxt!!\r\n", tag);
		xSemaphoreGive(cxtTbl->tblMutex);
		return ERROR;
	} else {
		LOG_DEBUG("In %s:cxtTblGetNextFreeCxtIdx: Take of tblMutex failed!!\r\n", tag);
		return ERROR;
	}
}

int16_t cxtTblAllocCxt(cxtTbl_t *cxtTbl, uint16_t cxtIdx) {
//	if (cxtTbl->tbl[cxtIdx].inUse == false) {
//		cxtTbl->tbl[cxtIdx].inUse = true;
//		LOG_DEBUG("In cxtTblAllocCxt: %d\r\n", cxtIdx);
//		return SUCCESS;
//	} else {
//		LOG_DEBUG("In cxtTblAllocCxt: Alloc failed!\r\n");
//		return ERROR;
//	}
	return SUCCESS;
}

int16_t cxtTblFreeCxt(cxtTbl_t *cxtTbl, uint16_t cxtIdx) {
	char *tag;

	if (cxtTbl == cStore.cpsmTbl) {
		tag = cpsmTag;
	} else {
		tag = hlfTag;
	}

	if ( xSemaphoreTake(cxtTbl->tblMutex, portMAX_DELAY) == pdTRUE ) {
	    //LOG_DEBUG("in %s:cxtTblFreeCxt\r\n", tag);

	    if (cxtTbl->tbl[cxtIdx].inUse == true) {
			cxtTbl->tbl[cxtIdx].inUse = false;
			cxtTbl->tbl[cxtIdx].ready = false;
			cxtTbl->tbl[cxtIdx].cmdType = CMD_NONE;
			cxtTbl->tbl[cxtIdx].cmdState = STATE_NONE;
			cxtTbl->tbl[cxtIdx].callingCPSMCxt = -1;
			cxtTbl->tbl[cxtIdx].callingArgCount = -1;
			cxtTbl->tbl[cxtIdx].returnArgCount = -1;
			LOG_DEBUG("In cxtTblFreeCxt: %d\r\n", cxtIdx);
			xSemaphoreGive(cxtTbl->tblMutex);
			return SUCCESS;
		} else {
			LOG_DEBUG("In %s:cxtTblFreeCxt: Free failed!\r\n", tag);
			xSemaphoreGive(cxtTbl->tblMutex);
			return ERROR;
		}
	} else {
		LOG_DEBUG("In %s:cxtTblFreeCxt: Take of tblMutex failed!!\r\n", tag);
		return ERROR;
	}
}

int16_t cxtTblGetNextReadyCxtIdx(cxtTbl_t *cxtTbl) {
	char *tag;

	if (cxtTbl == cStore.cpsmTbl) {
		tag = cpsmTag;
	} else {
		tag = hlfTag;
	}

	if ( xSemaphoreTake(cxtTbl->tblMutex, portMAX_DELAY) == pdTRUE ) {
	    //LOG_DEBUG("In %s:cxtTblGetNextReadyCxtIdx\r\n", tag);
		int16_t idx = 0;

		while (idx < MAX_TABLE_SIZE) {
			if (cxtTbl->tbl[idx].ready == true) {
				//Free cxt found, return idx
				LOG_INFO("In %s:cxtTblGetNextReadyCxtIdx: %d\r\n", tag, idx);
				xSemaphoreGive(cxtTbl->tblMutex);
				return idx;
			}
			idx = idx + 1;
		}
		//No ready cxt return error (-1);
		LOG_ERROR("In %s:cxtTblGetNextReadyCxtIdx:No ready cxt!!\r\n", tag);
		xSemaphoreGive(cxtTbl->tblMutex);
		return ERROR;
	} else {
		LOG_DEBUG("In %s:cxtTblGetNextReadyCxtIdx: Take of tblMutex failed!!\r\n", tag);
		return ERROR;
	}

}

int16_t cxtTblSetReadyCxtTrue(cxtTbl_t *cxtTbl, uint16_t cxtIdx) {
	UBaseType_t semCount;
	char *tag;

	if (cxtTbl == cStore.cpsmTbl) {
		tag = cpsmTag;
	} else {
		tag = hlfTag;
	}

	if ( xSemaphoreTake(cxtTbl->tblMutex, portMAX_DELAY) == pdTRUE ) {
		//LOG_DEBUG("in %s:cxtTblSetReadyCxt\r\n", tag);

		if (cxtTbl->tbl[cxtIdx].ready == false) {
			cxtTbl->tbl[cxtIdx].ready = true;
			LOG_DEBUG("In %s:cxtTblSetReadyCxt: true: %d\r\n", tag, cxtIdx);
			xSemaphoreGive( cxtTbl->cmdReadySem );
			semCount = uxSemaphoreGetCount( cxtTbl->cmdReadySem );
			LOG_DEBUG("In %s:cxtTblSetReadyCxt cmdReadySem: %d!\r\n", tag, semCount);
			xSemaphoreGive(cxtTbl->tblMutex);
			return SUCCESS;
		} else {
			LOG_DEBUG("In %s:cxtTblSetReadyCxt: logical error, Cxt %d ready state is already true!\r\n", tag, cxtIdx);
			xSemaphoreGive(cxtTbl->tblMutex);
			return ERROR;
		}
	} else {
		LOG_DEBUG("In %s:cxtTblSetReadyCxt: Take of tblMutex failed!!\r\n", tag);
		return ERROR;
	}

}

int16_t cxtTblSetReadyCxtFalse(cxtTbl_t *cxtTbl, uint16_t cxtIdx) {
	char *tag;

	if (cxtTbl == cStore.cpsmTbl) {
		tag = cpsmTag;
	} else {
		tag = hlfTag;
	}

	if ( xSemaphoreTake(cxtTbl->tblMutex, portMAX_DELAY) == pdTRUE ) {
		//LOG_DEBUG("In %s:cxtTblSetReadyCxt\r\n", tag);

		if (cxtTbl->tbl[cxtIdx].ready == true) {
			cxtTbl->tbl[cxtIdx].ready = false;
			LOG_DEBUG("In %s:cxtTblSetReadyCxt: false: %d\r\n", tag, cxtIdx);
			xSemaphoreGive(cxtTbl->tblMutex);
			return SUCCESS;
		} else {
			LOG_DEBUG("In %s:cxtTblSetReadyCxt: logical error, Cxt %d ready state is already false!\r\n", tag, cxtIdx);
			xSemaphoreGive(cxtTbl->tblMutex);
			return ERROR;
		}
	} else {
		LOG_DEBUG("In %s:cxtTblSetReadyCxt: Take of tblMutex failed!!\r\n", tag);
		return ERROR;
	}

}


