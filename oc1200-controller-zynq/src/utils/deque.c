/*
 * deque.c

 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */
#include "../../src/utils/deque.h"

#include <stdbool.h>

#include "../../src/utils/errorCode.h"
#include "../../src/utils/log.h"



qStore_t qStore;

deque_type * dequeInitHostInQueue() {
//    LOG_DEBUG("in dequeInitHostInQueue\r\n");
	deque_type *d = malloc(sizeof(deque_type));
//    LOG_DEBUG("td ptr: %p\r\n", d);

	d->queue_size = MAX_QUEUE_SIZE;
	d->queue_top = 0;
	d->queue_first = 0;
	for(uint8_t i = 0; i < d->queue_size; i++) {
		d->queue[i] = (queueCont_t*) malloc(sizeof(queueCont_t));
		memset(d->queue[i], 0, sizeof(queueCont_t));
//	    LOG_DEBUG("deque_alloc_hostIn_queue: td->queue[%d] ptr: %p\r\n", i, d->queue[i]);
	}

	//Init new cmd counting semaphore
	d->newCmdSem = xSemaphoreCreateCounting( MAX_QUEUE_SIZE, 0 );

	return d;
}

deque_type * dequeInitHLFInQueue() {
//    LOG_DEBUG("in dequeInitHLFInQueue\r\n");
	deque_type *d = malloc(sizeof(deque_type));
//    LOG_DEBUG("td ptr: %p\r\n", d);

	d->queue_size = MAX_QUEUE_SIZE;
	d->queue_top = 0;
	d->queue_first = 0;
	for(uint8_t i = 0; i < d->queue_size; i++) {
		d->queue[i] = (queueCont_t*) malloc(sizeof(queueCont_t));
		memset(d->queue[i], 0, sizeof(queueCont_t));
//	    LOG_DEBUG("dequeInitHLFInQueue: td->queue[%d] ptr: %p\r\n", i, d->queue[i]);
	}

	//Init new cmd counting semaphore
	d->newCmdSem = xSemaphoreCreateCounting( MAX_QUEUE_SIZE, 0 );

	return d;
}

deque_type * dequeInitAC12InQueue() {
//    LOG_DEBUG("in dequeInitAC12InQueue\r\n");
	deque_type *d = malloc(sizeof(deque_type));
//    LOG_DEBUG("td ptr: %p\r\n", d);

	d->queue_size = MAX_QUEUE_SIZE;
	d->queue_top = 0;
	d->queue_first = 0;
	for(uint8_t i = 0; i < d->queue_size; i++) {
		d->queue[i] = (queueCont_t*) malloc(sizeof(queueCont_t));
		memset(d->queue[i], 0, sizeof(queueCont_t));
//	    LOG_DEBUG("dequeAllocAC12InQueue: td->queue[%d] ptr: %p\r\n", i, d->queue[i]);
	}

	//Init new cmd counting semaphore
	d->newCmdSem = xSemaphoreCreateCounting( MAX_QUEUE_SIZE, 0 );

	return d;
}

deque_type * dequeInitAC12OutQueue() {
//    LOG_DEBUG("in dequeInitAC12OutQueue\r\n");
	deque_type *d = malloc(sizeof(deque_type));
//    LOG_DEBUG("td ptr: %p\r\n", d);

	d->queue_size = MAX_QUEUE_SIZE;
	d->queue_top = 0;
	d->queue_first = 0;
	for(uint8_t i = 0; i < d->queue_size; i++) {
		d->queue[i] = (queueCont_t*) malloc(sizeof(queueCont_t));
		memset(d->queue[i], 0, sizeof(queueCont_t));
//	    LOG_DEBUG("dequeAllocAC12OutQueue: td->queue[%d] ptr: %p\r\n", i, d->queue[i]);
	}

	//Init new cmd counting semaphore
	d->newCmdSem = xSemaphoreCreateCounting( MAX_QUEUE_SIZE, 0 );

	return d;
}


void dequeInitQueue(void) {
    LOG_DEBUG("in dequeInitQueue\r\n");
    qStore.ac12InQueue = dequeInitAC12InQueue();
//    LOG_DEBUG("in dequeInitAC12InQueue: ac12InQueue currentIndex %d\r\n",
//                    dequeGetCurrentIndex(qStore.ac12InQueue));
    qStore.ac12OutQueue = dequeInitAC12OutQueue();
//    LOG_DEBUG("in dequeInitAC12OutQueue: ac12OutQueue currentIndex %d\r\n",
//                    dequeGetCurrentIndex(qStore.ac12OutQueue));
    qStore.hostInQueue = dequeInitHostInQueue();
//    LOG_DEBUG("in dequeInitHostInQueue: hostInQueue currentIndex %d\r\n",
//                    dequeGetCurrentIndex(qStore.hostInQueue));
    qStore.hlfInQueue = dequeInitHLFInQueue();
//    LOG_DEBUG("in dequeInitHLFInQueue: hostOutQueue currentIndex %d\r\n",
//            dequeGetCurrentIndex(qStore.hlfInQueue));
}

/* Frees a deque_type pointed to by *d */
void dequeFree(deque_type *d);

int mod (int a, int b)
{
   int ret = a % b;
   if(ret < 0)
     ret+=b;
   return ret;
}

/* Returns true if there are no values in the deque, false otherwise. */
uint8_t dequeIsEmpty(deque_type *d) {
//    LOG_DEBUG("is_empty:top:first: %d, %d\r\n",d->queue_top, d->queue_top > 0, d->queue_first);
    if (d->queue_top == d->queue_first) {
    	return true;
    } else {
    	return false;
    }
}

/* Adds a new value to the front/back of the deque_type *d */
int8_t dequePush(deque_type *d) {

	int new_top = d->queue_top + 1;
	d->queue_top = mod(new_top, MAX_QUEUE_SIZE);
	if (d->queue_first ==  d->queue_top) {
		int new_first = d->queue_first + 1;
		d->queue_first = mod(new_first, MAX_QUEUE_SIZE);
	}
    //LOG_INFO("push t:f: %d:%d\r\n", d->queue_top, d->queue_first);
    return (d->queue_top);
}

int8_t dequePop(deque_type *d) {
	int new_top = d->queue_top - 1;
	d->queue_top = mod(new_top, MAX_QUEUE_SIZE);
    //LOG_INFO("pop t:f: %d:%d\r\n", d->queue_top, d->queue_first);
	return (d->queue_top);
}

int8_t dequeGetCurrentIndex(deque_type *d) {
//    LOG_DEBUG("deque_get_current_index: top: %d\r\n", d->queue_top);
//    LOG_DEBUG("deque_get_current_index: size: %d\r\n", d->queue_size);
    if (d->queue_top < d->queue_size) {
		return d->queue_top;
    } else {
        LOG_ERROR("**** Queue index overflow!\r\n");
    	return ERROR;
    }
}

