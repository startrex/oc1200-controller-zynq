/*
 * Log.h
 *
 *  Created on: Jan 17, 2018
 *      Author: pgrant
 */

#ifndef SRC_LOG_H_
#define SRC_LOG_H_
#include "xil_printf.h"

#define LOG_INFO    xil_printf
#define LOG_WARNING xil_printf
#define LOG_ERROR   xil_printf
#define LOG_DEBUG   xil_printf


#endif /* SRC_LOG_H_ */
