/*
 * cxtTbl.h
 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */

#ifndef SRC_CXTTBL_H_
#define SRC_CXTTBL_H_

#include "FreeRTOS.h"
#include "semphr.h"
#include "stdbool.h"

#define MAX_TABLE_SIZE  	15
#define MAX_CALLING_ARGS  	15
#define MAX_RETURN_ARGS  	15

#define CMD_NONE 			0x0000

#define STATE_NONE 			0x0000
#define STATE_INITDONE 		0x0001
#define STATE_TIDYUP 		0x0002
#define STATE_RELEASECXT 	0x0003

typedef struct cxt_s {
	bool 			isCPSMCxt;
	uint16_t 		cmdType;
	uint16_t		cmdState;
	bool            inUse;
	bool		    ready;
	int16_t			callingCPSMCxt;
	int8_t			callingArgCount;
	int32_t			callingArgs[MAX_CALLING_ARGS];
	int8_t			returnArgCount;
	int32_t			returnArgs[MAX_RETURN_ARGS];
	int32_t			state[MAX_RETURN_ARGS];
} cxt_t;


typedef struct cxtTbl_s {
	cxt_t 		 			tbl[MAX_TABLE_SIZE]; //Allocate maximum possible queue size
	SemaphoreHandle_t 		cmdReadySem;
	SemaphoreHandle_t 		tblMutex;
} cxtTbl_t;


typedef struct {
    int32_t            init;
    cxtTbl_t          *cpsmTbl;
    cxtTbl_t          *hlfTbl;
} cStore_t;

extern cStore_t cStore;

/* Instantiates a new deque_type.
 * Returns a pointer to a new deque_type with memory allocated by malloc,
 * returns NULL if it fails.
 */
void cxtTblInitCStore(void);

int16_t cxtTblGetNextFreeCxtIdx(cxtTbl_t *cxtTbl);

int16_t cxtTblAllocCxt(cxtTbl_t *cxtTbl, uint16_t cxtIdx);

int16_t cxtTblFreeCxt(cxtTbl_t *cxtTbl, uint16_t cxtIdx);

int16_t cxtTblGetNextReadyCxtIdx(cxtTbl_t *cxtTbl);

int16_t cxtTblSetReadyCxt(cxtTbl_t *cxtTbl, uint16_t cxtIdx);

#endif /* SRC_CXTTBL_H_ */
