/*
 * deque.h
 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */

#ifndef SRC_DEQUE_H_
#define SRC_DEQUE_H_

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "semphr.h"

#define MAX_QUEUE_SIZE  15

typedef void *deque_val_type;

typedef struct {
    uint16_t    cmdType;
    uint16_t    cmdArgCount;
    int32_t     cmdArgs[10];
} pciPkt_t;

typedef struct {
    uint16_t    timeStamp;
    pciPkt_t    pciPkt;
} queueCont_t;



/* deque_type is a deque object that the deque_foo functions
 * act upon.  New deque_type objects can be created with deque_alloc.
 */

typedef struct deque_struct {
	void 		 		*queue[MAX_QUEUE_SIZE]; //Allocatemaximum possible queue size
	int8_t	     		queue_size;
	int8_t       		queue_top;
	int8_t       		queue_first;
	SemaphoreHandle_t 	newCmdSem;
} deque_type;



/* Instantiates a new deque_type.
 * Returns a pointer to a new deque_type with memory allocated by malloc,
 * returns NULL if it fails.
 */
deque_type * dequeInitHostInQueue();
deque_type * dequeInitHLFInQueue();
deque_type * dequeInitAC12InQueue();
deque_type * dequeInitAC12OutQueue();

void dequeInitQueue(void);

/* Frees a deque_type pointed to by *d */
void dequeFree(deque_type *d);

/* Returns true if there are no values in the deque, false otherwise. */
uint8_t dequeIsEmpty(deque_type *d);

/* Adds a new value to the front/back of the deque_type *d */
int8_t dequePush(deque_type *d);

int8_t dequePop(deque_type *d);

int8_t dequeGetCurrentIndex(deque_type *d);


typedef struct {
    int32_t             init;
    deque_type          *hostInQueue;
    deque_type          *hlfInQueue;
    deque_type          *ac12InQueue;
    deque_type          *ac12OutQueue;
} qStore_t;

extern qStore_t qStore;


#endif /* SRC_DEQUE_H_ */
