/*
 * ahLib.c
 *
 *  Created on: Jan 15, 2018
 *      Author: pgrant
 */

#include "ahLib.h"

#include "../utils/deque.h"
#include "../utils/errorCode.h"
#include "../utils/log.h"

QueueHandle_t acoCallingQueue = NULL;

void ahDriverInit(void)
{
	LOG_INFO("In ahDriverInit: started\r\n");

	//Init AHDriver Qeueues
	acoCallingQueue = xQueueCreate( MAX_QUEUE_SIZE, sizeof( queueCont_t ) );

}

int16_t ahMDIOWrite(int16_t callingCxtIdx, uint32_t writeAddr, uint32_t writeValue)
{
	LOG_INFO("In ahMDIOWrite: started\r\n");

	ahPkt_t ahPkt;

	ahPkt.callingCxtIdx = callingCxtIdx;
	ahPkt.cmdType = AH_MDIO_WRITE;
	ahPkt.argCount = 2;
	ahPkt.args[0] = writeAddr;
	ahPkt.args[1] = writeValue;

	//Send cxtIdx to qeueue
    if( xQueueSend( acoCallingQueue, ( void * ) &ahPkt, ( TickType_t ) portMAX_DELAY ) != pdPASS )
    {
    	LOG_INFO("In ahMDIOWrite: xQueueSend failed!\r\n");
    	return ERROR;
    }

    return SUCCESS;

}

int16_t ahMDIORead(int16_t callingCxtIdx,  uint32_t readAddr)
{
	LOG_INFO("In ahMDIOWrite: started\r\n");

	ahPkt_t ahPkt;

	ahPkt.callingCxtIdx = callingCxtIdx;
	ahPkt.cmdType = AH_MDIO_READ;
	ahPkt.argCount = 1;
	ahPkt.args[0] = readAddr;

	//Send cxtIdx to qeueue
    if( xQueueSend( acoCallingQueue, ( void * ) &ahPkt, ( TickType_t ) portMAX_DELAY ) != pdPASS )
    {
    	LOG_INFO("In ahMDIORead: xQueueSend failed!\r\n");
    	return ERROR;
    }

    return SUCCESS;

}


