/*
 * ahLib.h
 *
 *  Created on: Jan 16, 2018
 *      Author: pgrant
 */

#ifndef SRC_AHLIB_H_
#define SRC_AHLIB_H_

#include "FreeRTOS.h"
#include "semphr.h"

void ahTask(void * arg);
void ahDriverInit(void);
int16_t ahMDIORead(int16_t callingCxtIdx,  uint32_t readAddr);
int16_t ahMDIOWrite(int16_t callingCxtIdx, uint32_t writeAddr, uint32_t writeValue);

#define MAX_ARGS  	15

//ah command types
#define AH_MDIO_WRITE  	0x0001
#define AH_MDIO_READ  	0x0002

#define INITIALVALUE 	0x00000000

typedef struct ahPkt_s {
	int16_t			callingCxtIdx;
	uint16_t		cmdType;
	int8_t			argCount;
	int32_t			args[MAX_ARGS];
} ahPkt_t;

QueueHandle_t acoCallingQueue;

#endif /* SRC_AHLIB_H_ */
